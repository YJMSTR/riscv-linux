> Corrector: [TinyCorrect](https://gitee.com/tinylab/tinycorrect) v0.1 - [pangu]<br/>
> Author:    Kepontry <Kepontry@163.com><br/>
> Date:      2023/2/27<br/>
> Revisor:   Falcon <falcon@tinylab.org>; Walimis <walimis@walimis.org><br/>
> Project:   [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)<br/>
> Proposal:  [VisionFive 2 开发板软硬件评测及软件 gap 分析](https://gitee.com/tinylab/riscv-linux/issues/I64ESM)<br/>
> Sponsor:   PLCT Lab, ISCAS

# 基于 VisionFive 2 的 Linux Kernel 编译与替换

## 简介

添加设备驱动，开启内核对某项功能的支持，或者获取最新特性，都需要重新编译 Linux 内核。内核编译后，需要将镜像文件和设备树文件拷贝至 `boot` 目录下，并设置启动项。编译内核在自己机器上或者开发板上都可以进行。本文介绍的是在自己的主机上开展交叉编译。

## 内核编译

### 准备工作

首先安装交叉编译工具链，然后从官方仓库克隆最新代码，并切换到 `JH7110_VisionFive2_devel` 分支，目前支持的是 5.15.0 版本的内核，官方会定期将新版软件 SDK 合并进来。

```shell
$ sudo apt update
$ sudo apt install gcc-riscv64-linux-gnu
$ cd ~
$ git clone https://github.com/starfive-tech/linux.git
$ cd linux
$ git checkout JH7110_VisionFive2_devel
```

### 内核配置与编译

接着生成内核配置文件，starfive 官方已经预先写好了一个默认配置文件，我们在指定交叉编译和架构后，编译生成 `.config` 文件。如果需要更改官方配置，可以在命令末尾加上 `menuconfig`，在图形界面中进行配置。

```shell
$ make starfive_visionfive2_defconfig CROSS_COMPILE=riscv64-linux-gnu- ARCH=riscv
  HOSTCC  scripts/basic/fixdep
  HOSTCC  scripts/kconfig/conf.o
  HOSTCC  scripts/kconfig/confdata.o
  HOSTCC  scripts/kconfig/expr.o
  LEX     scripts/kconfig/lexer.lex.c
  YACC    scripts/kconfig/parser.tab.[ch]
  HOSTCC  scripts/kconfig/lexer.lex.o
  HOSTCC  scripts/kconfig/menu.o
  HOSTCC  scripts/kconfig/parser.tab.o
  HOSTCC  scripts/kconfig/preprocess.o
  HOSTCC  scripts/kconfig/symbol.o
  HOSTCC  scripts/kconfig/util.o
  HOSTLD  scripts/kconfig/conf
#
# configuration written to .config
#
```

使用多核编译内核，编译时间在 20 分钟左右。

```shell
$ make CROSS_COMPILE=riscv64-linux-gnu- ARCH=riscv -j$(nproc)
  ......
  LD [M]  drivers/media/tuners/tea5767.ko
  LD [M]  fs/efivarfs/efivarfs.ko
  LD [M]  drivers/media/tuners/tuner-simple.ko
  LD [M]  drivers/media/tuners/tuner-types.ko
  LD [M]  drivers/media/spi/cxd2880-spi.ko
  LD [M]  drivers/media/tuners/tuner-xc2028.ko
```

在 `arch/riscv/boot` 目录下生成 `Image.gz` 内核镜像文件。

```shell
$ ls -lh arch/riscv/boot/
total 29M
-rwxr-xr-x. 1 root root  22M Feb 19 19:20 Image
-rw-r--r--. 1 root root 7.8M Feb 19 19:20 Image.gz
-rw-r--r--. 1 root root 1.6K Feb 19 18:20 Makefile
drwxr-xr-x. 6 root root  107 Feb 19 18:20 dts
-rw-r--r--. 1 root root 1.6K Feb 19 18:20 install.sh
-rw-r--r--. 1 root root  143 Feb 19 18:20 loader.S
-rw-r--r--. 1 root root  206 Feb 19 18:20 loader.lds.S
```

在 `arch/riscv/boot/dts/starfive` 目录下生成设备树文件，我们主要使用 `jh7110-visionfive-v2.dtb`。也可以根据设备类型，选用支持 `ac108` 或 `wm8960` 音频解码器的设备树文件。开发板启动时会根据启动项中设置的设备树目录 `fdtdir` 和 `/boot/uEnv.txt` 文件中设置的设备树文件名 `fdtfile`，选用对应的文件。

```shell
$ ls -lh arch/riscv/boot/dts/starfive/*.dtb
......
-rw-r--r--. 1 root root 47K Feb 19 19:17 jh7110-visionfive-v2-A10.dtb
-rw-r--r--. 1 root root 47K Feb 19 19:17 jh7110-visionfive-v2-A11.dtb
-rw-r--r--. 1 root root 48K Feb 19 19:17 jh7110-visionfive-v2-ac108.dtb
-rw-r--r--. 1 root root 48K Feb 19 19:17 jh7110-visionfive-v2-wm8960.dtb
-rw-r--r--. 1 root root 47K Feb 19 19:17 jh7110-visionfive-v2.dtb
```

### 生成内核文件

接着生成可引导的压缩内核文件 vmlinuz，通过设置 `INSTALL_PATH` 指定文件保存目录。

```shell
$ mkdir vmlinuz
$ make CROSS_COMPILE=riscv64-linux-gnu- ARCH=riscv INSTALL_PATH=~/linux/vmlinuz zinstall -j$(nproc)
$ ll ~/linux/vmlinuz
total 12340
drwxr-xr-x. 2 root root      90 Feb 19 20:28 ./
drwxr-xr-x. 7 root root     104 Feb 19 20:28 ../
-rw-r--r--. 1 root root 4362570 Feb 19 20:28 System.map-5.15.0
-rw-r--r--. 1 root root  146718 Feb 19 20:28 config-5.15.0
-rw-r--r--. 1 root root 8120315 Feb 19 20:28 vmlinuz-5.15.0
```

## 内核替换

### Boot 目录挂载

上文生成的内核文件 vmlinuz 和设备树文件 `jh7110-visionfive-v2.dtb` 需要复制到开发板的 `/boot` 目录下。如果在开发板上编译内核，则可以直接阅读下一章，将文件直接拷贝至对应目录即可。

如果在自己机器上编译内核，将 TF 卡插入读卡器后连接到电脑，通过 `df -h` 命令查看目录挂载详情，其中的 `/media/${USER}/root` 目录就是 TF 卡中的数据。但查看其中的 `boot` 目录，并没有发现文件。

```shell
$ df -h
Filesystem      Size  Used Avail Use% Mounted on
......
/dev/sdb3        16G   12G  3.6G  77% /media/user/root

$ ll /media/${USER}/root/boot
total 8
drwxr-xr-x  2 root root 4096 Jun 10  2022 ./
drwxr-xr-x 21 root root 4096 Jun 10  2022 ../
```

这是因为官方 Debian 镜像在烧写时默认将 `boot` 目录挂载在 TF 卡上的其它分区。通过 `sudo fdisk -l` 命令可查看插在电脑上的磁盘信息，通过容量（我的 TF 卡是 64G 的）和分区特征（从上面的 log 可以看到 sdb3 分区容量为 16G）可知 TF 卡的设备号为 sdb。

Linux 磁盘和分区的命名方式为，按检测顺序，依次将识别到的硬盘命名为 `/dev/sd*`，其中的星号为英文字母顺序。硬盘上的分区用阿拉伯数字进行编号，所以 `/dev/sdb2` 表示识别到的第二块硬盘的第二个分区，从下面的 log 可以看到，这是一个 100M 大小的 EFI 分区，挂载了用于启动的 `/boot` 目录。

```shell
$ sudo fdisk -l
......
Disk /dev/sdb: 59.49 GiB, 63864569856 bytes, 124735488 sectors
Disk model: Mass-Storage
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: B3E6F05B-DFC8-47B7-9C2B-F4762387709C

Device      Start      End  Sectors  Size Type
/dev/sdb1    2048    34815    32768   16M Linux filesystem
/dev/sdb2   34816   239615   204800  100M EFI System
/dev/sdb3  239616 32765918 32526303 15.5G Linux filesystem
```

接着通过 mount 命令将 `/dev/sdb2` 分区挂载到电脑的 `/mnt` 目录下（如果该目录已经挂载了其它分区就换一个目录），使我们能够读写该分区。Windows 端可以通过 DiskGenius 等工具查看并写入分区。

```shell
$ sudo mount /dev/sdb2 /mnt
$ cd /mnt
$ ll boot
total 21544
drwxr-xr-x 4 root root    2048 Dec 19 05:39 ./
drwxr-xr-x 3 root root   16384 Dec 31  1969 ../
-rwxr-xr-x 1 root root  142911 Dec 19 04:56 config-5.15.0-starfive*
drwxr-xr-x 4 root root    2048 Dec 19 05:39 dtbs/
drwxr-xr-x 2 root root    2048 Dec 19 06:50 extlinux/
-rwxr-xr-x 1 root root 9684953 Dec 19 05:24 initrd.img-5.15.0-starfive*
-rwxr-xr-x 1 root root 4191816 Dec 19 04:56 System.map-5.15.0-starfive*
-rwxr-xr-x 1 root root     385 Dec 19 05:24 uEnv.txt*
-rwxr-xr-x 1 root root 8015200 Dec 19 04:56 vmlinuz-5.15.0-starfive*
```

### 内核及设备树文件拷贝

最后进行文件拷贝，由于 `/boot` 目录下是 root 权限，写入、修改文件记得加上 sudo。由于 `uEnv.txt` 文件中指定的设备树文件为 `jh7110-visionfive-v2.dtb`，所以为简单起见，我们只覆盖这个文件，不覆盖其它的设备树文件。

```shell
$ sudo cp ~/linux/vmlinuz/vmlinuz-5.15.0 /mnt/boot/
$ sudo cp ~/linux/arch/riscv/boot/dts/starfive/jh7110-visionfive-v2.dtb /mnt/boot/dtbs/starfive/
```

最后修改 `boot/extlinux/extlinux.conf` 文件，为内核添加启动项。其中，`label` 用于设置该启动项的标签，`menu label` 是开机启动时内核选择界面所显示的名称，`linux` 项指定 vmlinuz 文件位置，其余部分保持与其它启动项一致即可。最后将文件开头的 `default` 设置为该启动相对应的标签，即 `l1`。

```shell
$ sudo vim boot/extlinux/extlinux.conf
# 添加以下内容
label l1
        menu label Debian-My
        linux /boot/vmlinuz-5.15.0
        initrd /boot/initrd.img-5.15.0-starfive
        fdtdir /boot/dtbs/
        append  root=/dev/mmcblk1p3 rw console=tty0 console=ttyS0,115200 earlycon rootwait stmmaceth=chain_mode:1 selinux=0
```

最后取消挂载，并将 TF 卡装回开发板上。

```shell
sudo umount /mnt
```

## 启动 VisionFive 2 开发板

启动开发板，接着进入 U-Boot 的 autoboot 流程，这里需要等待倒计时自动结束。

```shell
Hit any key to stop autoboot:  0
```

接着系统会列出识别到的内核，由于我们上面设置了默认启动项，所以默认启动新编译的内核。如果想要启动原来的内核，只需在选项框中输入对应数字并回车确认即可。

```shell
switch to partitions #0, OK
mmc1 is current device
Scanning mmc 1:2...
Found /boot/extlinux/extlinux.conf
Retrieving file: /boot/extlinux/extlinux.conf
1095 bytes read in 5 ms (213.9 KiB/s)
U-Boot menu
1:	Debian GNU/Linux bookworm/sid 5.15.0-starfive
2:	Debian GNU/Linux bookworm/sid 5.15.0-starfive (rescue target)
3:	Debian-My
Enter choice: 3
```

当出现如下信息，说明系统已经从我们更新的 `vmlinuz-5.15.0` 和 `jh7110-visionfive-v2.dtb` 启动。

```shell
Enter choice: 3
3:	Debian-My
Retrieving file: /boot/initrd.img-5.15.0-starfive
9684953 bytes read in 411 ms (22.5 MiB/s)
Retrieving file: /boot/vmlinuz-5.15.0
8120315 bytes read in 345 ms (22.4 MiB/s)
append: root=/dev/mmcblk1p3 rw console=tty0 console=ttyS0,115200 earlycon rootwait stmmaceth=chain_mode:1 selinux=0
Retrieving file: /boot/dtbs/starfive/jh7110-visionfive-v2.dtb
47809 bytes read in 8 ms (5.7 MiB/s)
   Uncompressing Kernel Image
Moving Image from 0x44000000 to 0x40200000, end=4176b000
## Flattened Device Tree blob at 48000000
   Booting using the fdt blob at 0x48000000
   Using Device Tree in place at 0000000048000000, end 000000004800eac0

Starting kernel ...
```

输入默认用户名 root 和密码 starfive 后，使用 `cat /proc/version` 命令查看版本信息。

```shell
$ cat /proc/version
Linux version 5.15.0 (root@670d3896bb83) (riscv64-linux-gnu-gcc (Ubuntu 9.4.0-1ubuntu1~20.04) 9.4.0, GNU ld (GNU Binutils for Ubuntu) 2.34) #1 SMP Sun Feb 19 19:17:58 CST 2023
```

## 总结

本文介绍了基于 VisionFive 2 的 Linux Kernel 编译与替换流程，为用户及时体验新特性，安装设备驱动提供帮助。

## 参考资料

- [昉·星光 2 单板计算机软件技术参考手册][001]

[001]: https://doc.rvspace.org/VisionFive2/PDF/VisionFive2_SW_TRM.pdf
