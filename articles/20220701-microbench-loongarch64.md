> Corrector: [TinyCorrect](https://gitee.com/tinylab/tinycorrect) v0.1-rc3 - [tounix spaces header urls]<br/>
> Author:  Zhou Zitang <1939566992@qq.com><br/>
> Date:    2022/07/01<br/>
> Revisor: Falcon <falcon@tinylab.org><br/>
> Project: [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)<br/>
> Sponsor: PLCT Lab, ISCAS

# 为 Microbench 添加 LoongArch64 支持

## 前言

本文主要介绍了为 microbench 添加 loongarch64 的详细过程。

## 安装软件包

首先，在 Ubuntu 下可以用 `apt` 命令安装 `git` 和 `vim`。

```
$ sudo apt install git vim
```

`sudo` 命令用于切换到管理员权限。

## 下载 microbench 源码

接下来从 `riscv-linux` 代码仓库中获取 microbench 源码：

```
$ git clone https://gitee.com/tinylab/riscv-linux.git
```

## 尝试编译 microbench

按照 `test/microbench/README.md` 文档的说明尝试编译：

```
$ cd riscv-linux/
$ cd test/microbench/
$ make
Makefile:43: *** Please add support for loongarch64, please refer to test/x86_64.cc and add new test/loongarch64.cc, cpumodel and product variables should be customized too.
```

编译后出现提示，说明该架构还不支持，需要进行专门适配。

## 适配 loongarch64 架构

根据提示，先从 `test/x86_64.cc` 拷贝一份 `test/loongarch64.cc`：

```
$ cp test/x86_64.cc test/loongarch64.cc
```

然后继续编译：

```
$ make
git clone https://gitee.com/mirrors/benchmark.git
正克隆到 'benchmark'...
remote: Enumerating objects: 7477, done.
remote: Counting objects: 100% (366/366), done.
remote: Compressing objects: 100% (342/342), done.
remote: Total 7477 (delta 199), reused 0 (delta 0), pack-reused 7111
接收对象中: 100% (7477/7477), 2.26 MiB | 785.00 KiB/s, 完成.
处理 delta 中: 100% (4995/4995), 完成.
ret=0; if [ -n "cmake" ]; then \
  if which apt >/dev/null; then \
    /usr/bin/sudo apt update -y; \
    /usr/bin/sudo apt install -y --no-install-recommends cmake || false; \
    ret=$?; \
  fi; \
  if which pacman >/dev/null; then \
    /usr/bin/sudo pacman -Sy; \
    /usr/bin/sudo pacman -S --noconfirm cmake || false; \
    ret=$?; \
  fi; \
  if which dnf >/dev/null; then \
    /usr/bin/sudo dnf makecache; \
    /usr/bin/sudo dnf install -y cmake || false; \
    ret=$?; \
  fi; \
fi; \
[ $ret -eq 0 ] || (echo "Err: missing 'cmake' and failed to install: 'cmake'" && false)
命中:1 http://pkg.loongnix.cn/loongnix DaoXiangHu-testing InRelease
正在读取软件包列表... 完成
正在分析软件包的依赖关系树
正在读取状态信息... 完成
有 2013 个软件包可以升级。请执行 ‘apt list --upgradable’ 来查看它们。
正在读取软件包列表... 完成
正在分析软件包的依赖关系树
正在读取状态信息... 完成
将会同时安装下列软件：
  cmake-data librhash0 libuv1
建议安装：
  cmake-doc ninja-build
下列【新】软件包将被安装：
  cmake cmake-data librhash0 libuv1
升级了 0 个软件包，新安装了 4 个软件包，要卸载 0 个软件包，有 2013 个软件包未被升级。
需要下载 4,792 kB 的归档。
解压缩后会消耗 24.3 MB 的额外空间。
获取:1 http://pkg.loongnix.cn/loongnix DaoXiangHu-testing/main loongarch64 cmake-data all 3.13.4-1.lnd.2 [1,477 kB]
获取:2 http://pkg.loongnix.cn/loongnix DaoXiangHu-testing/main loongarch64 librhash0 loongarch64 1.3.8-1.lnd.2 [128 kB]
获取:3 http://pkg.loongnix.cn/loongnix DaoXiangHu-testing/main loongarch64 libuv1 loongarch64 1.24.1-1+deb10u1.lnd.2 [110 kB]
获取:4 http://pkg.loongnix.cn/loongnix DaoXiangHu-testing/main loongarch64 cmake loongarch64 3.13.4-1.lnd.2 [3,078 kB]
已下载 4,792 kB，耗时 1秒 (4,544 kB/s)
正在选中未选择的软件包 cmake-data。
(正在读取数据库 ... 系统当前共安装有 226808 个文件和目录。)
准备解压 .../cmake-data_3.13.4-1.lnd.2_all.deb  ...
正在解压 cmake-data (3.13.4-1.lnd.2) ...
正在选中未选择的软件包 librhash0:loongarch64。
准备解压 .../librhash0_1.3.8-1.lnd.2_loongarch64.deb  ...
正在解压 librhash0:loongarch64 (1.3.8-1.lnd.2) ...
正在选中未选择的软件包 libuv1:loongarch64。
准备解压 .../libuv1_1.24.1-1+deb10u1.lnd.2_loongarch64.deb  ...
正在解压 libuv1:loongarch64 (1.24.1-1+deb10u1.lnd.2) ...
正在选中未选择的软件包 cmake。
准备解压 .../cmake_3.13.4-1.lnd.2_loongarch64.deb  ...
正在解压 cmake (3.13.4-1.lnd.2) ...
正在设置 libuv1:loongarch64 (1.24.1-1+deb10u1.lnd.2) ...
正在设置 librhash0:loongarch64 (1.3.8-1.lnd.2) ...
正在设置 cmake-data (3.13.4-1.lnd.2) ...
正在设置 cmake (3.13.4-1.lnd.2) ...
正在处理用于 man-db (2.8.5-2) 的触发器 ...
正在处理用于 libc-bin (2.28-10.lnd.29) 的触发器 ...
sed -i -e "s%https://github.com/google/googletest.git%https://gitee.com/mirrors/googletest.git%g" benchmark/WORKSPACE
sed -i -e "s%https://github.com/google/googletest.git%https://gitee.com/mirrors/googletest.git%g" benchmark/cmake/GoogleTest.cmake.in
cd benchmark && cmake -E make_directory "build"
cd benchmark && cmake -E chdir "build" cmake -DCMAKE_BUILD_TYPE=Release -DBENCHMARK_DOWNLOAD_DEPENDENCIES=on -DGOOGLETEST_PATH=/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_FLAGS="" -DCMAKE_CXX_FLAGS=""  ../
-- The CXX compiler identification is GNU 8.3.0
-- Check for working CXX compiler: /usr/bin/g++
-- Check for working CXX compiler: /usr/bin/g++ -- works
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Found Git: /usr/bin/git (found version "2.20.1")
-- git version: v1.6.1-59-g7280499e-dirty normalized to 1.6.1.59
-- Version: 1.6.1.59
-- Looking for shm_open in rt
-- Looking for shm_open in rt - found
-- Performing Test HAVE_CXX_FLAG_STD_CXX11
-- Performing Test HAVE_CXX_FLAG_STD_CXX11 - Success
-- Performing Test HAVE_CXX_FLAG_WALL
-- Performing Test HAVE_CXX_FLAG_WALL - Success
-- Performing Test HAVE_CXX_FLAG_WEXTRA
-- Performing Test HAVE_CXX_FLAG_WEXTRA - Success
-- Performing Test HAVE_CXX_FLAG_WSHADOW
-- Performing Test HAVE_CXX_FLAG_WSHADOW - Success
-- Performing Test HAVE_CXX_FLAG_WFLOAT_EQUAL
-- Performing Test HAVE_CXX_FLAG_WFLOAT_EQUAL - Success
-- Performing Test HAVE_CXX_FLAG_WERROR
-- Performing Test HAVE_CXX_FLAG_WERROR - Success
-- Performing Test HAVE_CXX_FLAG_PEDANTIC
-- Performing Test HAVE_CXX_FLAG_PEDANTIC - Success
-- Performing Test HAVE_CXX_FLAG_PEDANTIC_ERRORS
-- Performing Test HAVE_CXX_FLAG_PEDANTIC_ERRORS - Success
-- Performing Test HAVE_CXX_FLAG_WSHORTEN_64_TO_32
-- Performing Test HAVE_CXX_FLAG_WSHORTEN_64_TO_32 - Failed
-- Performing Test HAVE_CXX_FLAG_FSTRICT_ALIASING
-- Performing Test HAVE_CXX_FLAG_FSTRICT_ALIASING - Success
-- Performing Test HAVE_CXX_FLAG_WNO_DEPRECATED_DECLARATIONS
-- Performing Test HAVE_CXX_FLAG_WNO_DEPRECATED_DECLARATIONS - Success
-- Performing Test HAVE_CXX_FLAG_WNO_DEPRECATED
-- Performing Test HAVE_CXX_FLAG_WNO_DEPRECATED - Success
-- Performing Test HAVE_CXX_FLAG_WSTRICT_ALIASING
-- Performing Test HAVE_CXX_FLAG_WSTRICT_ALIASING - Success
-- Performing Test HAVE_CXX_FLAG_WD654
-- Performing Test HAVE_CXX_FLAG_WD654 - Failed
-- Performing Test HAVE_CXX_FLAG_WTHREAD_SAFETY
-- Performing Test HAVE_CXX_FLAG_WTHREAD_SAFETY - Failed
-- Performing Test HAVE_CXX_FLAG_COVERAGE
-- Performing Test HAVE_CXX_FLAG_COVERAGE - Success
-- Performing Test HAVE_STD_REGEX
-- Performing Test HAVE_STD_REGEX
-- Performing Test HAVE_STD_REGEX -- success
-- Performing Test HAVE_GNU_POSIX_REGEX
-- Performing Test HAVE_GNU_POSIX_REGEX
-- Performing Test HAVE_GNU_POSIX_REGEX -- failed to compile
-- Performing Test HAVE_POSIX_REGEX
-- Performing Test HAVE_POSIX_REGEX
-- Performing Test HAVE_POSIX_REGEX -- success
-- Performing Test HAVE_STEADY_CLOCK
-- Performing Test HAVE_STEADY_CLOCK
-- Performing Test HAVE_STEADY_CLOCK -- success
-- Looking for C++ include pthread.h
-- Looking for C++ include pthread.h - found
-- Looking for pthread_create
-- Looking for pthread_create - not found
-- Check if compiler accepts -pthread
-- Check if compiler accepts -pthread - yes
-- Found Threads: TRUE
-- Performing Test COMPILER_HAS_HIDDEN_VISIBILITY
-- Performing Test COMPILER_HAS_HIDDEN_VISIBILITY - Success
-- Performing Test COMPILER_HAS_HIDDEN_INLINE_VISIBILITY
-- Performing Test COMPILER_HAS_HIDDEN_INLINE_VISIBILITY - Success
-- Performing Test COMPILER_HAS_DEPRECATED_ATTR
-- Performing Test COMPILER_HAS_DEPRECATED_ATTR - Success
-- Looking for Google Test sources
-- Looking for Google Test sources in /home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest
CMake Warning at CMakeLists.txt:37 (message):
  Did not find Google Test sources! Fetching from web...

-- Configuring done
-- Generating done
-- Build files have been written to: /home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest
make[1]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest”
make[2]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest”
make[3]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest”
Scanning dependencies of target googletest
make[3]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest”
make[3]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest”
[ 11%] Creating directories for 'googletest'
[ 22%] Performing download step (git clone) for 'googletest'
正克隆到 'src'...
注意：正在检出 'release-1.11.0'。

您正处于分离头指针状态。您可以查看、做试验性的修改及提交，并且您可以通过另外
的检出分支操作丢弃在这个状态下所做的任何提交。

如果您想要通过创建分支来保留在此状态下所做的提交，您可以通过在检出命令添加
参数 -b 来实现（现在或稍后）。例如：

  git checkout -b <新分支名>

HEAD 目前位于 e2239ee6 Googletest export
[ 33%] No patch step for 'googletest'
[ 44%] Performing update step for 'googletest'
[ 55%] No configure step for 'googletest'
[ 66%] No build step for 'googletest'
[ 77%] No install step for 'googletest'
[ 88%] No test step for 'googletest'
[100%] Completed 'googletest'
make[3]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest”
[100%] Built target googletest
make[2]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest”
make[1]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest”
-- The C compiler identification is GNU 8.3.0
-- Check for working C compiler: /usr/bin/gcc
-- Check for working C compiler: /usr/bin/gcc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Found Python: /usr/bin/python3.7 (found version "3.7.3") found components:  Interpreter
-- Performing Test BENCHMARK_HAS_O3_FLAG
-- Performing Test BENCHMARK_HAS_O3_FLAG - Success
-- Performing Test BENCHMARK_HAS_WNO_ODR
-- Performing Test BENCHMARK_HAS_WNO_ODR - Success
-- Performing Test BENCHMARK_HAS_WNO_LTO_TYPE_MISMATCH
-- Performing Test BENCHMARK_HAS_WNO_LTO_TYPE_MISMATCH - Success
-- Configuring done
-- Generating done
-- Build files have been written to: /home/loongson/Develop/riscv-linux/test/microbench/benchmark/build
cd benchmark/test && cp ../../test/loongarch64.cc loongarch64.cc
grep "define OPTIMIZE_LEVEL 1" benchmark/test/loongarch64.cc || \
sed -i -e "s/define OPTIMIZE_LEVEL.*/define OPTIMIZE_LEVEL 1/g" benchmark/test/loongarch64.cc
#define OPTIMIZE_LEVEL 1
cd benchmark && \
git checkout -- test/CMakeLists.txt && \
sed -i -e "/compile_benchmark_test(basic_test)/icompile_benchmark_test(loongarch64)" test/CMakeLists.txt && \
sed -i -e "/compile_benchmark_test(basic_test)/iadd_test(NAME loongarch64 COMMAND loongarch64 --benchmark_min_time=0.01)\n" test/CMakeLists.txt
touch benchmark/test/loongarch64.dep
cd benchmark && cmake -DCMAKE_BUILD_TYPE=Release -DBENCHMARK_DOWNLOAD_DEPENDENCIES=on -DGOOGLETEST_PATH=/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_FLAGS="" -DCMAKE_CXX_FLAGS=""  -S . -B "build"
-- git version: v1.6.1-59-g7280499e-dirty normalized to 1.6.1.59
-- Version: 1.6.1.59
-- Performing Test HAVE_STD_REGEX -- success
-- Performing Test HAVE_GNU_POSIX_REGEX -- failed to compile
-- Performing Test HAVE_POSIX_REGEX -- success
-- Performing Test HAVE_STEADY_CLOCK -- success
-- Looking for Google Test sources
-- Looking for Google Test sources in /home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest
CMake Warning at CMakeLists.txt:37 (message):
  Did not find Google Test sources! Fetching from web...

-- Configuring done
-- Generating done
-- Build files have been written to: /home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest
make[1]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest”
make[2]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest”
make[3]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest”
make[3]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest”
make[3]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest”
[ 11%] Performing update step for 'googletest'
[ 22%] No configure step for 'googletest'
[ 33%] No build step for 'googletest'
[ 44%] No install step for 'googletest'
[ 55%] No test step for 'googletest'
[ 66%] Completed 'googletest'
make[3]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest”
[100%] Built target googletest
make[2]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest”
make[1]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build/third_party/googletest”
-- Configuring done
-- Generating done
-- Build files have been written to: /home/loongson/Develop/riscv-linux/test/microbench/benchmark/build
cd benchmark && cmake --build "build" --config Release --target loongarch64
make[1]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
make[2]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
make[3]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
make[4]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
Scanning dependencies of target benchmark
make[4]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
make[4]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
[  4%] Building CXX object src/CMakeFiles/benchmark.dir/benchmark.cc.o
[  8%] Building CXX object src/CMakeFiles/benchmark.dir/benchmark_api_internal.cc.o
[ 13%] Building CXX object src/CMakeFiles/benchmark.dir/benchmark_name.cc.o
[ 17%] Building CXX object src/CMakeFiles/benchmark.dir/benchmark_register.cc.o
[ 21%] Building CXX object src/CMakeFiles/benchmark.dir/benchmark_runner.cc.o
[ 26%] Building CXX object src/CMakeFiles/benchmark.dir/check.cc.o
[ 30%] Building CXX object src/CMakeFiles/benchmark.dir/colorprint.cc.o
[ 34%] Building CXX object src/CMakeFiles/benchmark.dir/commandlineflags.cc.o
[ 34%] Building CXX object src/CMakeFiles/benchmark.dir/complexity.cc.o
[ 39%] Building CXX object src/CMakeFiles/benchmark.dir/console_reporter.cc.o
[ 43%] Building CXX object src/CMakeFiles/benchmark.dir/counter.cc.o
[ 47%] Building CXX object src/CMakeFiles/benchmark.dir/csv_reporter.cc.o
[ 52%] Building CXX object src/CMakeFiles/benchmark.dir/json_reporter.cc.o
[ 56%] Building CXX object src/CMakeFiles/benchmark.dir/perf_counters.cc.o
[ 60%] Building CXX object src/CMakeFiles/benchmark.dir/reporter.cc.o
[ 65%] Building CXX object src/CMakeFiles/benchmark.dir/sleep.cc.o
[ 69%] Building CXX object src/CMakeFiles/benchmark.dir/statistics.cc.o
[ 73%] Building CXX object src/CMakeFiles/benchmark.dir/string_util.cc.o
[ 78%] Building CXX object src/CMakeFiles/benchmark.dir/sysinfo.cc.o
[ 82%] Building CXX object src/CMakeFiles/benchmark.dir/timers.cc.o
[ 82%] Linking CXX static library libbenchmark.a
make[4]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
[ 82%] Built target benchmark
make[4]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
Scanning dependencies of target benchmark_main
make[4]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
make[4]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
[ 86%] Building CXX object src/CMakeFiles/benchmark_main.dir/benchmark_main.cc.o
[ 91%] Linking CXX static library libbenchmark_main.a
make[4]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
[ 91%] Built target benchmark_main
make[4]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
Scanning dependencies of target loongarch64
make[4]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
make[4]: 进入目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
[ 95%] Building CXX object test/CMakeFiles/loongarch64.dir/loongarch64.cc.o
/tmp/ccdDuwmW.s: Assembler messages:
/tmp/ccdDuwmW.s:267: 致命错误：no match insn: jmp	2f
make[4]: *** [test/CMakeFiles/loongarch64.dir/build.make:63：test/CMakeFiles/loongarch64.dir/loongarch64.cc.o] 错误 1
make[4]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
make[3]: *** [CMakeFiles/Makefile2:764：test/CMakeFiles/loongarch64.dir/all] 错误 2
make[3]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
make[2]: *** [CMakeFiles/Makefile2:776：test/CMakeFiles/loongarch64.dir/rule] 错误 2
make[2]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
make[1]: *** [Makefile:357：loongarch64] 错误 2
make[1]: 离开目录“/home/loongson/Develop/riscv-linux/test/microbench/benchmark/build”
make: *** [Makefile:138：benchmark/build/test/loongarch64] 错误 2
```

从上述日志，可以看到已经完成了部分编译，但是最终出错了。关键错误信息为：

```
[ 95%] Building CXX object test/CMakeFiles/loongarch64.dir/loongarch64.cc.o
/tmp/ccdDuwmW.s: Assembler messages:
/tmp/ccdDuwmW.s:267: 致命错误：no match insn: jmp	2f
```

然后，继续查看文档说明：

```
$ cat README.md
...
And then, refer to the target ISA Spec and customize the instructions in `test/$(ARCH).cc`.
...
```

然后我们去查看龙芯处理器架构的手册，可以从 [这里][001] 下载下来。

通过查看手册，我们在该文档的 “2.2 基础整数指令概述” 中可以找到 “转移指令”，其中有：

* 无条件跳转指令：B
* 条件跳转指令：BEQZ 和 BNEZ

然后，我们把 X86_64 架构中的 `jmp` 指令替换为龙芯架构的 `b` 指令：

```
$ gedit test/loongarch64.cc
$ diff -Nubr test/x86_64.cc test/loongarch64.cc
--- test/x86_64.cc	2022-06-30 19:42:29.838897314 +0800
+++ test/loongarch64.cc	2022-06-30 20:11:01.109928459 +0800
@@ -31,7 +31,7 @@
     for (auto _ : state) {
         benchmark_DoNotOptimize();
         asm volatile (
-            "1: jmp 2f \n"
+            "1: b 2f \n"
             "2:"
             :::"memory");
     }
```

接下来继续编译，可以预期还是会有错误：

```
$ make
[ 95%] Building CXX object test/CMakeFiles/loongarch64.dir/loongarch64.cc.o
/tmp/ccOXMTCR.s: Assembler messages:
/tmp/ccOXMTCR.s:346: 致命错误：no match insn: cmp	$0x0,$r12
```

然后依葫芦画瓢，把另外两条指令也做替换。这个过程中我们发现龙芯的这两条指令跟 RISCV64 架构相似，所以参考了 `test/riscv64.cc` 做修改，最终修改效果如下：

```
$ diff -Nubr test/x86_64.cc test/loongarch64.cc
--- test/x86_64.cc	2022-06-30 19:42:29.838897314 +0800
+++ test/loongarch64.cc	2022-06-30 20:11:01.109928459 +0800
@@ -31,7 +31,7 @@
     for (auto _ : state) {
         benchmark_DoNotOptimize();
         asm volatile (
-            "1: jmp 2f \n"
+            "1: b 2f \n"
             "2:"
             :::"memory");
     }
@@ -45,8 +45,7 @@
     for (auto _ : state) {
         benchmark_DoNotOptimize();
         asm volatile (
-            "cmp $0x0, %0 \n"
-            "1: jnz 2f \n"
+            "1: bnez %0, 2f \n"
             "2:"
             :
             :"r" (x)
@@ -62,8 +61,7 @@
     for (auto _ : state) {
         benchmark_DoNotOptimize();
         asm volatile (
-            "cmp $0x0, %0 \n"
-            "1: jz 2f \n"
+            "1: beqz %0, 2f \n"
             "2:"
             :
             :"r" (x)
```

最后再做测试验证，顺利通过测试：

```
$ make
benchmark/build/test/loongarch64
2022-06-30T20:33:26+08:00
Running benchmark/build/test/loongarch64
Run on (4 X 2300 MHz CPU s)
CPU Caches:
  L1 Data 64 KiB (x4)
  L1 Instruction 64 KiB (x4)
  L2 Unified 256 KiB (x4)
  L3 Unified 16384 KiB (x1)
Load Average: 1.20, 1.24, 1.20
-------------------------------------------------------------------------
Benchmark                               Time             CPU   Iterations
-------------------------------------------------------------------------
BM_nop                              0.873 ns        0.870 ns    803813290
BM_ub                                1.31 ns         1.31 ns    536321245
BM_bnez                             0.871 ns        0.870 ns    804546654
BM_beqz                             0.870 ns        0.870 ns    804586603
BM_load_bnez                         1.74 ns         1.74 ns    406520283
BM_load_beqz                         1.74 ns         1.74 ns    402303960
BM_cache_miss_load_bnez              3.51 ns         3.51 ns    199302237
BM_cache_miss_load_beqz              3.50 ns         3.50 ns    199940138
BM_branch_miss_load_bnez             3.66 ns         3.66 ns    199592774
BM_branch_miss_load_beqz             3.58 ns         3.58 ns    199589592
BM_cache_branch_miss_load_bnez       3.62 ns         3.62 ns    199425808
BM_cache_branch_miss_load_beqz       3.51 ns         3.51 ns    196872556
```

## 完善 logging 支持

回到首次编译的提示信息：

```
Makefile:43: *** Please add support for loongarch64, please refer to test/x86_64.cc and add new test/loongarch64.cc, cpumodel and product variables should be customized too.
```

我们还需要在 Makefile 中增加代码获取 cpumodel 和 product 信息，增加后变成这样：

```
$ git diff
diff --git a/test/microbench/Makefile b/test/microbench/Makefile
index 8459a15..b17c36b 100644
--- a/test/microbench/Makefile
+++ b/test/microbench/Makefile
@@ -69,6 +69,11 @@ ifneq ($(filter $(ARCH),armv7 armv7l aarch64),)
   endif
 endif

+ifeq ($(ARCH),loongarch64)
+  cpumodel   := $$(egrep -m3 -i 'model name|features' /proc/cpuinfo  | tr '\t' ' ' | tr -s ' ' | cut -d ':' -f2 | sed -e "s/^ //g"| tr -c '[a-zA-Z0-9\.]' '-' | tr -s '-' | sed -e 's/-$$//g' | cut -c1-60)
+  product    := $$(cat /sys/class/dmi/id/product_name | cut -d ' ' -f1)
+endif
+
 ifneq ($(wildcard benchmark/test/$(TEST_BIN).cc),)
   ifeq ($(shell grep "define OPTIMIZE_LEVEL $(OPTIMIZE_LEVEL)" benchmark/test/$(TEST_BIN).cc),)
     dummy := $(shell sed -i -e "s/define OPTIMIZE_LEVEL.*/define OPTIMIZE_LEVEL $(OPTIMIZE_LEVEL)/g" benchmark/test/$(TEST_BIN).cc)
```

测试验证：

```
$ make logging
$ ls logs/ | grep LS3A5000
LM-LS3A5000-7A1000-1w-V01-pc_A2101-Loongson-3A5000LL-cpucfg-lam-ual-fpu-lsx-lasx-complex-crypto-loongarch64-20220630-204148-O1.log
```

可以看到，确实生成了带有 product 和 cpumodel 详细信息的日志文件。

可以按照文档同时生成一份不优化的测试数据：

```
$ make logging O=0
benchmark/build/test/loongarch64
2022-06-30T20:46:46+08:00
Running benchmark/build/test/loongarch64
Run on (4 X 2300 MHz CPU s)
CPU Caches:
  L1 Data 64 KiB (x4)
  L1 Instruction 64 KiB (x4)
  L2 Unified 256 KiB (x4)
  L3 Unified 16384 KiB (x1)
Load Average: 1.63, 1.34, 1.23
-------------------------------------------------------------------------
Benchmark                               Time             CPU   Iterations
-------------------------------------------------------------------------
BM_nop                               1.74 ns         1.74 ns    402184159
BM_ub                                2.18 ns         2.18 ns    321757598
BM_bnez                              1.74 ns         1.74 ns    402277581
BM_beqz                              2.17 ns         2.17 ns    321828619
BM_load_bnez                         1.74 ns         1.74 ns    402279523
BM_load_beqz                         1.74 ns         1.74 ns    402299683
BM_cache_miss_load_bnez              4.97 ns         4.97 ns    138197736
BM_cache_miss_load_beqz              4.83 ns         4.82 ns    146674303
BM_branch_miss_load_bnez             4.88 ns         4.88 ns    142110535
BM_branch_miss_load_beqz             4.93 ns         4.93 ns    145057905
BM_cache_branch_miss_load_bnez       5.75 ns         5.75 ns    142097746
BM_cache_branch_miss_load_beqz       8.09 ns         8.09 ns    100000000
$ ls logs/ | grep LS3A5000
LM-LS3A5000-7A1000-1w-V01-pc_A2101-Loongson-3A5000LL-cpucfg-lam-ual-fpu-lsx-lasx-complex-crypto-loongarch64-20220630-204148-O1.log
LM-LS3A5000-7A1000-1w-V01-pc_A2101-Loongson-3A5000LL-cpucfg-lam-ual-fpu-lsx-lasx-complex-crypto-loongarch64-20220630-204309-O0.log
```

## 小结

到这里为止，就为 microbench 添加了 loongarch64 处理器支持并跑出了测试数据。

感兴趣的同学，可以通过 `logs/` 下的数据对比分析 loongarch64 架构跟其他架构指令级层面的性能差异。

## 参考资料

* [龙芯架构参考手册 - 卷一：基础架构][002]
* test/microbench/README.md

[001]: https://github.com/loongson/LoongArch-Documentation/releases/latest/download/LoongArch-Vol1-v1.00-CN.pdf
[002]: https://loongson.github.io/LoongArch-Documentation/README-CN.html
