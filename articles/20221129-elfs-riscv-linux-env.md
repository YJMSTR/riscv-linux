> Corrector: [TinyCorrect](https://gitee.com/tinylab/tinycorrect) v0.1 - [header toc tables urls autocorrect]<br/>
> Author: Zhou zitang<br/>
> Date: 2022/11/29<br/>
> Revisor: Bin Meng, IosDevLog<br/>
> Project: [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)<br/>
> Proposal: [从零开始嵌入式 Linux (RISC-V + Linux v6.x)][004]<br/>
> Sponsor: PLCT Lab, ISCAS

# 搭建实验环境：RISC-V Lab 和 Linux Lab

## 简介

### 背景介绍

泰晓社区今年开展了一系列 [RISC-V Linux 内核技术调研][007] 活动。几个月以来，数十位同学一起学习、交流、协作，陆续输出了上百篇分析文章，开展了近 50 次在线视频直播分享，并逐渐往上游项目社区贡献代码。

该活动目前的门槛相对比较高一些，主要面向有一定经验的同学。在活动开展的过程中，泰晓社区收到了很多同学的反馈，有些同学希望推荐一些门槛相对低一点的入门文章。为了满足更多社区用户的需要，泰晓社区决定启动一个《从零开始嵌入式 Linux（基于 RISC-V + Linux v6.x）》专项，该系列基于 RISC-V 处理器架构和 Linux 内核 v6.x 展开，以便帮助更多的同学快速上手。

该专项信息如下：

- 提案：[从零开始嵌入式 Linux (RISC-V + Linux v6.x)][004]
- 仓库：<https://gitee.com/tinylab/elfs>
- 文章：所有文字类成果目前还是统一提交到 RISC-V Linux 仓库的 articles/ 目录下，后续可能会组织线上技术分享，上述仓库用于公开课的组织与开展。

欢迎同学们在提案后面回复认领感兴趣的章节，也可以回复提出各种需求与建议。

### 本文说明

这篇文章是该系列的第一篇，主要介绍实验环境的搭建。

该《从零开始嵌入式 Linux（基于 RISC-V + Linux v6.x）》专项是一个实操性很强的学习活动，所以我们把实验环境的准备安排在第一篇文章里头，建议大家首先根据本文把实验环境准备好，后面就可以直接跟着一起学习，一起做实验，一起讨论。

嵌入式 Linux 开发涉及到硬件基础、Linux 系统使用、程序设计、内核开发与驱动开发等知识，为了降低大家的实验门槛，泰晓社区专门开发了两套开源实验环境：RISC-V Lab 和 Linux Lab：

- 其中，RISC-V Lab 主要用于嵌入式 Linux 软件开发，比如说学习 Linux 系统用法、C 语言、RISC-V 汇编、RISC-V 工具移植等。RISC-V Lab 可以直接跨架构在 X86 主机上运行，不仅省去了购买 RISC-V 硬件的环节，也可以避免交叉编译等相对繁杂的操作。项目首页：<https://gitee.com/tinylab/riscv-lab>

- 而 Linux Lab 主要用于嵌入式 Linux 内核与驱动方面的学习与开发，当然，也可以用来构建文件系统、学习 U-Boot 引导器以及 RISC-V 模拟器等。Linux Lab 同样可以直接跨架构在 X86 主机上运行，集成了 riscv64/virt 和 riscv32/virt 两款虚拟开发板。项目首页：<https://gitee.com/tinylab/linux-lab>

本文主要就是带领大家一起来安装 RISC-V Lab 和 Linux Lab，并简单演示它们的用法。更详细的用法，我们会在后面的文章中陆续展开介绍，大家感兴趣也可以通过上面介绍的项目首页去提前自学。

在开始实验之前，我们需要准备好一个基本的 Linux 操作系统环境。

## 实验准备

### 安装 Linux 系统

RISC-V Lab 和 Linux Lab 这两套实验环境都基于 Docker，所以，它们并不依赖特定的操作系统，但是建议大家尽量在 Linux 操作系统下使用，可以获得更好的兼容性体验，避免奇奇怪怪的问题。

如果大家手头没有 Linux 系统，那么建议直接选购一个泰晓社区研发的 Linux 实验盘，

![泰晓 Linux 实验盘](images/elfs/linux-lab-disk1.jpg)

该实验盘免安装，可以即插即跑，支持自动备份和自助恢复，而且默认已经集成了用于运行 Linux Lab 和 RISC-V Lab 的实验管理软件：[Cloud Lab][001]，所以能够直接运行 Linux Lab 和 RISC-V Lab。另外，像 docker, git, vim 等基本的开发工具也已经提前安装好了，无需额外安装。

如需选购泰晓 Linux 实验盘，可以直接访问泰晓开源小店：<https://shop155917374.taobao.com>，也可以直接在淘宝应用中检索 “泰晓 Linux” 关键字。如需了解该实验盘的功能和用法，可以访问：<https://tinylab.org/linux-lab-disk>

为了保持一致性，建议大家优先选用泰晓 Linux 实验盘，或者自行准备 Ubuntu 20.04 系统。

![泰晓 Linux 实验盘 - 使用效果](images/elfs/linux-lab-disk2.jpg)

### 安装基本开发工具

如果没有选购泰晓 Linux 实验盘，那么需要自行安装 git, vim, docker 等，以 Ubuntu 为例：

```
$ sudo apt install -y git vim docker.io
```

### 安装 Cloud Lab

上面介绍到，RISC-V Lab 和 Linux Lab 都是通过 [Cloud Lab][001] 实验管理软件来运行的，该软件也由泰晓社区研发。

同样地，如果没有选购泰晓 Linux 实验盘，那么需要先自行下载 Cloud Lab 并进入 Cloud Lab 的工作目录：

```
$ git clone https://gitee.com/tinylab/cloud-lab.git
$ cd cloud-lab
```

## 嵌入式 Linux 应用开发：RISC-V Lab 用法

本节介绍专门面向嵌入式 Linux 应用开发的 RISC-V Lab 实验环境。

### RISC-V Lab 简介

根据 RISC-V Lab 项目的 [仓库首页][003]，我们知道：

> RISC-V Lab 可以于 10 秒内在 X86 主机上跨架构跑起一个 RISC-V 桌面系统，基础系统是 Ubuntu 22.04，已支持 lxqt, xfce 桌面，并内置有 gcc, gdb 等开发工具，适合 RISC-V 本地开发，能有效避免交叉编译的烦恼。

### 运行 RISC-V Lab

在 Cloud Lab 的工作目录下，可以通过 `tools/docker/run` 工具直接运行 RISC-V Lab。

需要注意的是：在首次运行过程中，Cloud Lab 会协助自动下载 RISC-V Lab 的实验镜像，该镜像文件有点大，稍微需要等一段时间。而在泰晓 Linux 实验盘中，已经配置好了加速镜像，所以下载应该会很快完成。

接下来，直接运行：

```
$ tools/docker/run riscv-lab
LOG: Init docker environment ...
INFO: Resume the stopped riscv-lab
INFO: Run host side init for riscv-lab
INFO: Start riscv-lab-25651-4a9553
riscv-lab-25651-4a9553
INFO: Wait for lab launching
INFO: Wait for lab resuming...
```

### 选择登录方式

运行完以后，RISC-V Lab 会提供几种登陆方式供用户选择：

- bash：常见的命令行方式
- vnc：VNC 图形界面登陆方式
- ssh：远程命令行登陆方式，可以同时启动图形应用
- webssh：浏览器中的命令行登陆方式
- webvnc：浏览器中的图形界面登陆方式

我们这里选择比较简单的 bash 命令行登陆方式：

```
INFO: Please choose one of the login methods:

     1	bash
     2	vnc
     3	ssh
     4	webssh
     5	webvnc

INFO: Choose the login method: 1

     1	bash

INFO: Available login methods:

     bash vnc ssh webssh webvnc

INFO: Switch to another method:

     tools/docker/login LOGIN_METHOD

INFO: Running '/home/ubuntu/Develop/cloud-lab/tools/docker/bash'

ubuntu@riscv-lab:/labs/riscv-lab$ ls
```

正常情况下，都无需手动输入登陆账号和密码。登陆完成以后，会弹出一个可以输入命令的命令行提示符。

如需开展图形应用开发，建议选用 vnc 和 webvnc 登陆方式。

### 查看系统架构与版本

登陆进去以后，就是一个完整的 RISC-V Ubuntu 22.04 操作系统，我们可以用来学习 Linux 基本命令和 C 语言编程，也可以用来学习 RISC-V 汇编，还可以用来做 RISC-V 软件开发与移植。

下面简单演示几个命令：

```
ubuntu@riscv-lab:/labs/riscv-lab$ ls
COPYING  README.en.md  README.md
ubuntu@riscv-lab:/labs/riscv-lab$ cat /etc/issues
Ubuntu 22.04 LTS \n \l

ubuntu@riscv-lab:/labs/riscv-lab$ uname -a
Linux riscv-lab 5.15.0-27-generic #28-Ubuntu SMP Thu Apr 14 04:55:28 UTC 2022 riscv64 riscv64 riscv64 GNU/Linux
```

通过上述命令，我们就能确认当前运行的确实是一个 riscv64 的 Ubuntu 22.04 操作系统。

### 查看基本开发工具

RISC-V Lab 也已经预装了常用的开发工具，比如 git, vim, gcc, gdb 等：

```
ubuntu@riscv-lab:/labs/riscv-lab$ git --version
git version 2.34.1
ubuntu@riscv-lab:/labs/riscv-lab$ which vim
/usr/bin/vim
ubuntu@riscv-lab:/labs/riscv-lab$ gcc --version
gcc (Ubuntu 11.2.0-19ubuntu1) 11.2.0
Copyright (C) 2021 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

ubuntu@riscv-lab:/labs/riscv-lab$ gdb --version
GNU gdb (Ubuntu 12.0.90-0ubuntu1) 12.0.90
Copyright (C) 2022 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
```

这些工具的用法咱们这里就不做展开，有待我们在后续文章中慢慢介绍。

如需其他的工具，也可以自行通过 `apt` 命令来安装。

## 嵌入式 Linux 内核开发：Linux Lab 用法

本节介绍专门面向嵌入式 Linux 内核开发的 Linux Lab 实验环境。

### Linux Lab 简介

同样地，根据 Linux Lab 项目的 [仓库首页][003]，我们知道：

> 本项目致力于创建一个基于 Docker + QEMU 的 Linux 实验环境，方便大家学习、开发和测试 Linux 内核。

另外，上述仓库还介绍到 Linux Lab 具有如下功能：

|编号| 特性       |  描述                                                                                |
|----|------------|--------------------------------------------------------------------------------------|
|1   | 开发板     | 基于 QEMU，支持 7+ 主流体系架构，20+ 款流行虚拟开发板；支持多款真实开发板            |
|2   | 组件       | 支持 U-Boot，Linux, Buildroot，QEMU。支持 Linux v0.11, v2.6.10 ~ v5.x                |
|3   | 预置组件   | 提供上述组件的预先编译版本，并按开发板分类存放，可即时下载使用                       |
|4   | 根文件系统 | 支持 initrd，harddisk，mmc 和 nfs; ARM 架构提供 Debian 系统                          |
|5   | Docker     | 包括 gcc-4.3 在内的交叉工具链已预先安装，还可灵活配置并下载外部交叉工具链            |
|6   | 灵活访问   | 支持本地或网络访问，支持命令行和图形界面，支持 bash, ssh, vnc, web ssh, web vnc      |
|7   | 网络       | 内置桥接网络支持，每个开发板都支持网络（Raspi3 是唯一例外）                          |
|8   | 启动       | 支持串口、Curses（用于 `bash/ssh` 访问）和图形化方式启动                             |
|9   | 测试       | 支持通过 `make test` 命令对目标板进行自动化测试                                      |
|10  | 调试       | 可通过 `make debug` 命令对目标板进行调试                                             |

本文仅带领大家简单上手。

### 运行 Linux Lab

同样通过 Cloud Lab 下的 `tools/docker/run` 工具直接运行 Linux Lab。

需要注意的是：在首次运行过程中，Cloud Lab 会协助自动下载 Linux Lab 的实验镜像，该镜像文件有点大，稍微需要等一段时间。而在泰晓 Linux 实验盘中，已经直接集成了实验镜像，也已经提前下载好了 Linux 内核源码，所以能在 1 分钟内直接上手。

接下来，直接运行：

```
$ tools/docker/run linux-lab
LOG: Init docker environment ...
INFO: Open the running linux-lab
```

由上可见，其运行方式跟 RISC-V Lab 基本一致。

### 选择登录方式

接下来，可以参考 RISC-V Lab 一节的介绍来选择登陆方式，这里同样选择 bash：

```
INFO: Please choose one of the login methods:

     1	bash
     2	vnc
     3	ssh
     4	webssh
     5	webvnc

INFO: Choose the login method: 1

     1	bash

INFO: Available login methods:

     bash vnc ssh webssh webvnc

INFO: Switch to another method:

     tools/docker/login LOGIN_METHOD

INFO: Running '/home/ubuntu/Develop/cloud-lab/tools/docker/bash'

ubuntu@linux-lab:/labs/linux-lab$
```

如需开展图形相关的 Linux 内核实验，建议选用 vnc 和 webvnc 这两种支持图形界面的方式。

### 查看支持的开发板

Linux Lab 已经支持 7 大主流处理器架构和 20 多款真实或虚拟开发板，可以直接通过 `make list` 或者 `make list-short` 命令查看支持的开发板列表：

```
ubuntu@linux-lab:/labs/linux-lab$ make list-short
[ aarch64/raspi3 ]:
      ARCH    := arm64
      LINUX   ?= v5.1
[ aarch64/virt ]:
      ARCH    := arm64
      LINUX   ?= v5.13
      LINUX[KERNEL_FORK_openeuler]   ?= 5.10.0-5.10.0
[ arm/mcimx6ul-evk ]:
      ARCH    := arm
      LINUX   ?= v5.4
[ arm/versatilepb ]:
      ARCH    := arm
      LINUX   ?= v5.1
[ arm/vexpress-a9 ]:
      ARCH    := arm
      LINUX   ?= v6.0.7
[ i386/pc ]:
      ARCH    := x86
      LINUX   ?= v5.13
[ mips64el/loongson3-virt ]:
      ARCH    := mips
      LINUX   ?= v5.18
[ mips64el/ls2k ]:
      ARCH    := mips
      LINUX   ?= loongnix-release-1903
      LINUX[LINUX_loongnix-release-1903] := 04b98684
[ mips64el/ls3a7a ]:
      ARCH    := mips
      LINUX   ?= loongnix-release-1903
      LINUX[LINUX_loongnix-release-1903] := 04b98684
[ mipsel/ls1b ]:
      ARCH    := mips
      LINUX   ?= v5.2
[ mipsel/ls232 ]:
      ARCH    := mips
      LINUX   ?= v2.6.32-r190726
[ mipsel/malta ]:
      ARCH    := mips
      LINUX   ?= v5.13
[ ppc/g3beige ]:
      ARCH    := powerpc
      LINUX   ?= v5.13
[ riscv32/virt ]:
      ARCH    := riscv
      LINUX   ?= v5.17
[ riscv64/virt ]:
      ARCH    := riscv
      LINUX   ?= v5.18.9
[ x86_64/pc ]:
      ARCH    := x86
      LINUX   ?= v5.13
      LINUX[KERNEL_FORK_openeuler]   ?= 5.10.0-5.10.0
[ arm/ebf-imx6ull ]:
      ARCH    := arm
      LINUX   ?= v4.19.35
```

### 选择某块开发板

Linux Lab 默认启用了 `arm/vexpress-a9` 开发板，如需切换，以 `riscv64/virt` 为例，可以这样：

```
$ make BOARD=riscv64/virt
```

需要注意的是：由于开发与维护成本过高，目前仅开放 `arm/vexpress-a9` 开发板，如需其他开发板，大家同样可以根据提示去泰晓开源小店选购 Linux Lab BSP，也可以直接在淘宝应用检索 “泰晓 Linux” 关键字。

### 引导内核和文件系统

在选中某块开发板以后，可以直接通过 `make boot` 命令引导预编译好的 U-boot、Linux 内核和文件系统：

```
ubuntu@linux-lab:/labs/linux-lab$ make boot
LOG: Generating rootfs image for uboot ...
Image Name:
Created:      Tue Nov 29 23:27:23 2022
Image Type:   ARM Linux RAMDisk Image (uncompressed)
Data Size:    767409 Bytes = 749.42 KiB = 0.73 MiB
Load Address: 00000000
Entry Point:  00000000
sudo   qemu-system-arm  -M vexpress-a9 -cpu cortex-a9 -m 128M -net nic,model=lan9118 -net tap -smp 1 -kernel /labs/linux-lab/boards/arm/vexpress-a9/bsp/uboot/v2020.04/u-boot -no-reboot  -drive if=pflash,file=tftpboot/pflash.img,format=raw -nographic
ALSA lib pulse.c:242:(pulse_connect) PulseAudio: Unable to connect: Access denied
...
U-Boot 2020.04-dirty (May 06 2020 - 16:17:30 +0800)

DRAM:  128 MiB
WARNING: Caches not enabled
Flash: 128 MiB
MMC:   MMC: 0
*** Warning - bad CRC, using default environment

In:    serial
Out:   serial
Err:   serial
Net:   smc911x-0
Hit any key to stop autoboot:  0
## Warning: defaulting to text format
## Booting kernel from Legacy Image at 60003000 ...
   Image Name:   Linux-5.17.0
   Image Type:   ARM Linux Kernel Image (uncompressed)
   Data Size:    5137000 Bytes = 4.9 MiB
   Load Address: 60003000
   Entry Point:  60003000
   Verifying Checksum ... OK
## Loading init Ramdisk from Legacy Image at 60900000 ...
   Image Name:
   Image Type:   ARM Linux RAMDisk Image (uncompressed)
   Data Size:    767409 Bytes = 749.4 KiB
   Load Address: 00000000
   Entry Point:  00000000
   Verifying Checksum ... OK
## Flattened Device Tree blob at 60500000
   Booting using the fdt blob at 0x60500000
   Loading Kernel Image
   Loading Ramdisk to 67dba000, end 67e755b1 ... OK
   Loading Device Tree to 67db3000, end 67db9700 ... OK

Starting kernel ...

Booting Linux on physical CPU 0x0
Linux version 5.17.0 (ubuntu@linux-lab) (arm-linux-gnueabi-gcc (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0, GNU ld (GNU Binutils for Ubuntu) 2.34) #1 SMP Tue Mar 22 20:40:19 CST 2022
CPU: ARMv7 Processor [410fc090] revision 0 (ARMv7), cr=10c5387d
CPU: PIPT / VIPT nonaliasing data cache, VIPT nonaliasing instruction cache
OF: fdt: Machine model: V2P-CA9
OF: fdt: Ignoring memory block 0x80000000 - 0x80000004
Memory policy: Data cache writeback
Reserved memory: created DMA memory pool at 0x4c000000, size 8 MiB
OF: reserved mem: initialized node vram@4c000000, compatible id shared-dma-pool
Zone ranges:
  Normal   [mem 0x0000000060000000-0x0000000067ffffff]
Movable zone start for each node
Early memory node ranges
  node   0: [mem 0x0000000060000000-0x0000000067ffffff]
Initmem setup node 0 [mem 0x0000000060000000-0x0000000067ffffff]
CPU: All CPU(s) started in SVC mode.
percpu: Embedded 15 pages/cpu s30028 r8192 d23220 u61440
Built 1 zonelists, mobility grouping on.  Total pages: 32512
Kernel command line: route=172.20.67.86 iface=eth0 root=/dev/ram0 console=ttyAMA0
Unknown kernel command line parameters "route=172.20.67.86 iface=eth0", will be passed to user space.
printk: log_buf_len individual max cpu contribution: 4096 bytes
printk: log_buf_len total cpu_extra contributions: 12288 bytes
printk: log_buf_len min size: 16384 bytes
printk: log_buf_len: 32768 bytes
printk: early log buf free: 14816(90%)
Dentry cache hash table entries: 16384 (order: 4, 65536 bytes, linear)
Inode-cache hash table entries: 8192 (order: 3, 32768 bytes, linear)
mem auto-init: stack:off, heap alloc:off, heap free:off
Memory: 116616K/131072K available (8192K kernel code, 695K rwdata, 1748K rodata, 1024K init, 151K bss, 14456K reserved, 0K cma-reserved)
SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=4, Nodes=1
ftrace: allocating 27708 entries in 82 pages
ftrace: allocated 82 pages with 3 groups
trace event string verifier disabled
rcu: Hierarchical RCU implementation.
rcu: 	RCU restricting CPUs from NR_CPUS=8 to nr_cpu_ids=4.
	Rude variant of Tasks RCU enabled.
rcu: RCU calculated value of scheduler-enlistment delay is 10 jiffies.
rcu: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=4
NR_IRQS: 16, nr_irqs: 16, preallocated irqs: 16
GIC CPU mask not found - kernel will fail to boot.
GIC CPU mask not found - kernel will fail to boot.
L2C: platform modifies aux control register: 0x02020000 -> 0x02420000
L2C: DT/platform modifies aux control register: 0x02020000 -> 0x02420000
L2C-310 enabling early BRESP for Cortex-A9
L2C-310 full line of zeros enabled for Cortex-A9
L2C-310 dynamic clock gating disabled, standby mode disabled
L2C-310 cache controller enabled, 8 ways, 128 kB
L2C-310: CACHE_ID 0x410000c8, AUX_CTRL 0x46420001
random: get_random_bytes called from start_kernel+0x52c/0x6dc with crng_init=0
sched_clock: 32 bits at 24MHz, resolution 41ns, wraps every 89478484971ns
clocksource: arm,sp804: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 1911260446275 ns
Failed to initialize '/bus@40000000/motherboard-bus@40000000/iofpga@7,00000000/timer@12000': -22
smp_twd: clock not found -2
Console: colour dummy device 80x30
Calibrating local timer... 91.67MHz.
Calibrating delay loop... 1265.66 BogoMIPS (lpj=6328320)
pid_max: default: 32768 minimum: 301
Mount-cache hash table entries: 1024 (order: 0, 4096 bytes, linear)
Mountpoint-cache hash table entries: 1024 (order: 0, 4096 bytes, linear)
CPU: Testing write buffer coherency: ok
CPU0: Spectre v2: using BPIALL workaround
CPU0: thread -1, cpu 0, socket 0, mpidr 80000000
cblist_init_generic: Setting adjustable number of callback queues.
cblist_init_generic: Setting shift to 2 and lim to 1.
Setting up static identity map for 0x60100000 - 0x60100060
rcu: Hierarchical SRCU implementation.
smp: Bringing up secondary CPUs ...
smp: Brought up 1 node, 1 CPU
SMP: Total of 1 processors activated (1265.66 BogoMIPS).
CPU: All CPU(s) started in SVC mode.
devtmpfs: initialized
VFP support v0.3: implementor 41 architecture 3 part 30 variant 9 rev 0
clocksource: jiffies: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 19112604462750000 ns
futex hash table entries: 1024 (order: 4, 65536 bytes, linear)
NET: Registered PF_NETLINK/PF_ROUTE protocol family
DMA: preallocated 256 KiB pool for atomic coherent allocations
cpuidle: using governor ladder
hw-breakpoint: debug architecture 0x4 unsupported.
Serial: AMBA PL011 UART driver
vgaarb: loaded
SCSI subsystem initialized
usbcore: registered new interface driver usbfs
usbcore: registered new interface driver hub
usbcore: registered new device driver usb
pps_core: LinuxPPS API ver. 1 registered
pps_core: Software ver. 5.3.6 - Copyright 2005-2007 Rodolfo Giometti <giometti@linux.it>
PTP clock support registered
Advanced Linux Sound Architecture Driver Initialized.
clocksource: Switched to clocksource arm,sp804
NET: Registered PF_INET protocol family
IP idents hash table entries: 2048 (order: 2, 16384 bytes, linear)
tcp_listen_portaddr_hash hash table entries: 512 (order: 0, 6144 bytes, linear)
TCP established hash table entries: 1024 (order: 0, 4096 bytes, linear)
TCP bind hash table entries: 1024 (order: 1, 8192 bytes, linear)
TCP: Hash tables configured (established 1024 bind 1024)
UDP hash table entries: 256 (order: 1, 8192 bytes, linear)
UDP-Lite hash table entries: 256 (order: 1, 8192 bytes, linear)
NET: Registered PF_UNIX/PF_LOCAL protocol family
RPC: Registered named UNIX socket transport module.
RPC: Registered udp transport module.
RPC: Registered tcp transport module.
RPC: Registered tcp NFSv4.1 backchannel transport module.
PCI: CLS 0 bytes, default 64
Unpacking initramfs...
hw perfevents: enabled with armv7_cortex_a9 PMU driver, 5 counters available
workingset: timestamp_bits=30 max_order=15 bucket_order=0
squashfs: version 4.0 (2009/01/31) Phillip Lougher
jffs2: version 2.2. (NAND) © 2001-2006 Red Hat, Inc.
9p: Installing v9fs 9p2000 file system support
io scheduler mq-deadline registered
io scheduler kyber registered
Freeing initrd memory: 752K
physmap-flash 40000000.flash: physmap platform flash device: [mem 0x40000000-0x43ffffff]
40000000.flash: Found 2 x16 devices at 0x0 in 32-bit bank. Manufacturer ID 0x000000 Chip ID 0x000000
Intel/Sharp Extended Query Table at 0x0031
Using buffer write method
physmap-flash 40000000.flash: physmap platform flash device: [mem 0x44000000-0x47ffffff]
40000000.flash: Found 2 x16 devices at 0x0 in 32-bit bank. Manufacturer ID 0x000000 Chip ID 0x000000
Intel/Sharp Extended Query Table at 0x0031
Using buffer write method
Concatenating MTD devices:
(0): "40000000.flash"
(1): "40000000.flash"
into device "40000000.flash"
physmap-flash 48000000.psram: physmap platform flash device: [mem 0x48000000-0x49ffffff]
smsc911x 4e000000.ethernet eth0: MAC Address: 52:54:00:12:34:56
isp1760 4f000000.usb: isp1760 bus width: 32, oc: digital
isp1760 4f000000.usb: NXP ISP1760 USB Host Controller
isp1760 4f000000.usb: new USB bus registered, assigned bus number 1
isp1760 4f000000.usb: Scratch test failed. 0x00000000
isp1760 4f000000.usb: can't setup: -19
isp1760 4f000000.usb: USB bus 1 deregistered
usbcore: registered new interface driver usb-storage
ledtrig-cpu: registered to indicate activity on CPUs
usbcore: registered new interface driver usbhid
usbhid: USB HID core driver
NET: Registered PF_PACKET protocol family
9pnet: Installing 9P2000 support
Registering SWP/SWPB emulation handler
aaci-pl041 10004000.aaci: ARM AC'97 Interface PL041 rev0 at 0x10004000, irq 34
aaci-pl041 10004000.aaci: FIFO 512 entries
mmci-pl18x 10005000.mmci: Got CD GPIO
mmci-pl18x 10005000.mmci: Got WP GPIO
mmci-pl18x 10005000.mmci: mmc0: PL181 manf 41 rev0 at 0x10005000 irq 35,36 (pio)
10009000.uart: ttyAMA0 at MMIO 0x10009000 (irq = 39, base_baud = 0) is a PL011 rev1
printk: console [ttyAMA0] enabled
1000a000.uart: ttyAMA1 at MMIO 0x1000a000 (irq = 40, base_baud = 0) is a PL011 rev1
1000b000.uart: ttyAMA2 at MMIO 0x1000b000 (irq = 41, base_baud = 0) is a PL011 rev1
1000c000.uart: ttyAMA3 at MMIO 0x1000c000 (irq = 42, base_baud = 0) is a PL011 rev1
rtc-pl031 10017000.rtc: registered as rtc0
rtc-pl031 10017000.rtc: setting system clock to 2022-11-29T15:27:28 UTC (1669735648)
amba 10020000.clcd: Fixing up cyclic dependency with 0-0039
clcd-pl11x 10020000.clcd: PL111 designer 41 rev2 at 0x10020000
clcd-pl11x: probe of 10020000.clcd failed with error -2
ALSA device list:
  #0: ARM AC'97 Interface PL041 rev0 at 0x10004000, irq 34
Freeing unused kernel image (initmem) memory: 1024K
input: AT Raw Set 2 keyboard as /devices/platform/bus@40000000/bus@40000000:motherboard-bus@40000000/bus@40000000:motherboard-bus@40000000:iofpga@7,00000000/10006000.kmi/serio0/input/input0
Run /init as init process
Starting syslogd: input: ImExPS/2 Generic Explorer Mouse as /devices/platform/bus@40000000/bus@40000000:motherboard-bus@40000000/bus@40000000:motherboard-bus@40000000:iofpga@7,00000000/10007000.kmi/serio1/input/input2
OK
Starting klogd: OK
Running sysctl: OK
Saving random seed: random: dd: uninitialized urandom read (512 bytes read)
OK
Starting network: Generic PHY 4e000000.ethernet-ffffffff:01: attached PHY driver (mii_bus:phy_addr=4e000000.ethernet-ffffffff:01, irq=POLL)
smsc911x 4e000000.ethernet eth0: SMSC911x/921x identified at 0x8c910000, IRQ: 26
eth: eth0, ip: 172.20.67.68, gw: 172.20.67.86
OK

Welcome to Linux Lab

linux-lab login: root
#
# uname -a
Linux linux-lab 6.0.7 #1 SMP Wed Nov 9 14:57:28 CST 2022 armv7l GNU/Linux
#
# poweroff
#
Stopping network: OK
Saving random seed: OK
Stopping klogd: OK
Stopping syslogd: OK
umount: devtmpfs busy - remounted read-only
umount: can't unmount /: Invalid argument
The system is going down NOW!
Sent SIGTERM to all processes
Sent SIGKILL to all processes
Requesting system poweroff
Flash device refused suspend due to active operation (state 20)
Flash device refused suspend due to active operation (state 20)
reboot: Power down
```

### 其他实验

我们还可以通过 `make kernel` 直接编译某个版本的 Linux 内核，开展某项 Linux 内核特性的实验，也可以用于学习最新的 Linux v6.x 内核的新技术，可以很方便地开展调试与测试。

更多的用法本文就不再做展开，大家可以提前看 Linux Lab 项目首页的文档，也可以在后续的文章中跟我们一起来开展实验。

## 总结

通过这篇文章，我们顺利准备好了整个《从零开始嵌入式 Linux（基于 RISC-V + Linux v6.x）》专项所需的实验环境，不仅有面向嵌入式应用开发的 RISC-V Lab，也有面向嵌入式 Linux 内核开发的 Linux Lab。

在后续文章中，我们将基于这两套实验环境开展各项实验，更多内容且听下回分解。

如果对该系列感兴趣，赶紧联系我们吧，联系微信：tinylab。

## 参考资料

- [Cloud Lab][001]
- [Linux Lab][002]
- [RISC-V Lab][003]
- [泰晓 Linux 实验盘][006]
- [泰晓开源小店][005]

[001]: https://gitee.com/tinylab/cloud-lab
[002]: https://gitee.com/tinylab/linux-lab
[003]: https://gitee.com/tinylab/riscv-lab
[004]: https://gitee.com/tinylab/riscv-linux/issues/I61K05
[005]: https://shop155917374.taobao.com/
[006]: https://tinylab.org/linux-lab-disk
[007]: https://tinylab.org/riscv-linux
