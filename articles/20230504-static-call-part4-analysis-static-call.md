> Corrector: [TinyCorrect](https://gitee.com/tinylab/tinycorrect) v0.1 - [spaces codeinline urls]<br/>
> Author:    牛工 - 通天塔 985400330@qq.com<br/>
> Date:      2023/05/04<br/>
> Revisor:   Falcon <falcon@ruma.tech>; iOSDevLog <iosdevlog@iosdevlog.com><br/>
> Project:   [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)<br/>
> Proposal:  [【老师提案】Static Call 技术分析与 RISC-V 移植 · Issue #I5Y585 · 泰晓科技/RISCV-Linux - Gitee.com](https://gitee.com/tinylab/riscv-linux/issues/I5Y585)<br/>
> Sponsor:   PLCT Lab, ISCAS

# Static Call 系列（4）：为 RISC-V 架构移植 static call

## 前言

在文章 [RISC-V 缺失的 Linux 内核功能-Part2][004] 中，提到了 RISC-V 架构下缺失的内核功能：Avoiding retpolines with Static Call。

该功能的提出，是有历史原因的：

- 2018 年发现漏洞 Meltdown 和 Spectre
- 谷歌提出 Retpolines 解决了这个安全问题，但引入了 4% 的性能影响
- 开发者们不断寻求解决方法：[Relief for retpoline pain][002]
- 2020 年引入 Static Call 方法来在某些场合避免使用 retpolines，性能影响降低至 1.6%：[Avoiding retpolines with static calls][003]

该系列的前 3 篇文章，讲了 Meltdown 和 Spectre、retpolines 原理以及 Static Call 源码分析。

本篇文章对 Static Call 移植过程进行记录。

## 移植前的准备工作

### 搭建编译环境

Linux版本：v6.3-rc4

实验环境：[Linux Lab](https://tinylab.org/linux-lab)

### 需要移植的文件


* 头文件
  * arch/riscv/include/asm/static_call.h

* C 文件
  * arch/riscv/kernel/static_call.c

* 编译及配置文件
  * arch/riscv/Kconfig
  * arch/riscv/kernel/Makefile

### 需要移植的函数

Static Call common 部分函数：

- `DEFINE_STATIC_CALL` 声明等宏定义
- `__static_call_init`
- `__static_call_update`
- `static_call_key`
- `static_call_addr`

Static Call 与架构相关的关键函数：

- `void arch_static_call_transform(void *site, void *tramp, void *func)` 用于指令转换，用于实现 Static Call 之后的函数切换。

- `ARCH_DEFINE_STATIC_CALL_TRAMP` 宏定义的实现，用于定义永久蹦床。

## 开始移植

### 永久蹦床定义

永久蹦床的定义很重要的一项就是选择合适的寄存器，其中有个概念就是调用者保存寄存器，可以供子函数自由使用，而不用考虑调用者保存的问题。

本次选择的寄存器是 t0，t0 寄存器常用于临时变量的保存，不需要考虑函数退出时恢复原数值，属于调用者保存寄存器。

```assembly
diff --git a/arch/riscv/include/asm/static_call.h b/arch/riscv/include/asm/static_call.h
new file mode 100644
index 000000000000..8fa80e0ea16d
--- /dev/null
+++ b/arch/riscv/include/asm/static_call.h
@@ -0,0 +1,30 @@
+/* SPDX-License-Identifier: GPL-2.0 */
+#ifndef _ASM_STATIC_CALL_H
+#define _ASM_STATIC_CALL_H
+#include <linux/ftrace.h>
+
+#define __ARCH_DEFINE_STATIC_CALL_TRAMP(name, insns)                   \
+       asm(".pushsection .static_call.text, \"ax\"     \n"     \
+           ".globl " STATIC_CALL_TRAMP_STR(name) "     \n"     \
+           STATIC_CALL_TRAMP_STR(name) ":              \n"     \
+               insns "                                 \n"     \
+           ".type " STATIC_CALL_TRAMP_STR(name) ", @function\n"        \
+           ".size " STATIC_CALL_TRAMP_STR(name) ", . - " STATIC_CALL_TRAMP_STR(name) "\n" \
+           ".popsection                                                        \n")
+
+#define ARCH_DEFINE_STATIC_CALL_TRAMP(name, func)                      \
+       __ARCH_DEFINE_STATIC_CALL_TRAMP(name, "la t0,"#func";jalr t1,0(t0);")
+
+#define ARCH_DEFINE_STATIC_CALL_NULL_TRAMP(name)                       \
+       __ARCH_DEFINE_STATIC_CALL_TRAMP(name, "ret;")
+
+#define ARCH_DEFINE_STATIC_CALL_RET0_TRAMP(name)                       \
+       ARCH_DEFINE_STATIC_CALL_TRAMP(name, __static_call_return0;)
+
+#define ARCH_ADD_TRAMP_KEY(name)                                       \
+       asm(".pushsection .static_call_tramp_key, \"a\" \n"     \
+           ".long " STATIC_CALL_TRAMP_STR(name) " - .  \n"     \
+           ".long " STATIC_CALL_KEY_STR(name) " - .    \n"     \
+           ".popsection                                \n")
+
+#endif /* _ASM_STATIC_CALL_H */

```

蹦床的主要作用是：作为固定的地址，用于函数的跳转。蹦床代码可以被修改，通过修改蹦床的汇编代码，实现目标函数地址的切换。

主要替换的代码：`la t0,"#func";jalr t1,0(t0);`

替换代码的工作由 `arch_static_call_transform()` 进行实现。

### arch_static_call_transform函数实现

代码实现于 `arch/riscv/kernel/static_call.c`。

`__static_call_transform()` 是一个用于实现静态函数调用优化的函数，它的作用是将静态函数调用指令（`static_call`）转换为相应的直接跳转指令（`jump`）。

该函数的输入参数含义如下：

- `insn`：指向静态函数调用指令的地址。静态函数调用指令是一种特殊的调用指令，其使用一个类似于函数指针的机制，将调用目标的地址存储在指令中。该参数指向这个指令的地址。
- `type`：指示指令类型的枚举值。这个参数用于判断静态函数调用指令的类型，并将其转换为相应的跳转指令类型。
- `func`：目标函数的地址。这个参数是静态函数调用指令中存储的函数指针指向的地址，即被调用函数的入口地址。

`__static_call_transform()` 函数的主要工作是根据指令类型参数 `type` 将静态函数调用指令转换为相应的跳转指令。

当前主要实现了 `type` 为 `JUMP` 时，表示静态函数调用指令是一个无条件跳转指令，此时 `__static_call_transform()` 将指令替换为新的跳转指令，该指令可以直接跳转到 `func` 参数指定的目标函数地址。

由于 `__static_call_transform()` 函数是一个内部函数，一般情况下不需要直接调用它，而是通过调用公共接口 `arch_static_call_transform` 来实现静态函数调用升级。

由于risc-v一条指令无法完成32位地址的跳转，只能实现12位的跳转，导致不得不考虑两条指令完成函数调用。

这就导致了需要考虑多核情况下的代码变更导致的跳转异常的情况。

所以必须实现更改第一条跳转指令时，不能影响其原有的跳转逻辑；更换第二条指令时，立马生效新的跳转逻辑。

#### 方案设计

方案使用了3条指令进行了逻辑设计：

1. 改变第一条指令，不影响原跳转逻辑

![image-20230504224902144](images/static-call/image-20230504224902144.png)

2. 随意修改悬空状态的指令，修改完成后，再次修改指令，完成函数调用完整指令。

![image-20230504230406740](images/static-call/image-20230504230406740.png)

以上完成了蹦床初始化，到第一次蹦床升级的指令变化。

第二次蹦床升级流程如下：

1. 改变第一条指令，不影响原跳转逻辑

2. 修改跳转指令

![image-20230504230535434](images/static-call/image-20230504230535434.png)

第三次蹦床升级流程如下：

1. 改变第一条指令（悬空指令），不影响原跳转逻辑

2. 修改原跳转指令，使第三条指令不再悬空

![image-20230504230604980](images/static-call/image-20230504230604980.png)

#### 代码实现

具体代码实现如下：

```c
+static void __ref __static_call_transform(void *insn, enum insn_type type, void *func, bool modinit)
+{
+       u32 call[2] = {0};
+       u32 jump_code = (*(u32 *)(insn+sizeof(call[0])) & GENMASK(6, 0));
+
+       switch (type) {
+               case CALL:
+                       break;
+               case NOP:
+                       break;
+               case JMP:
+                       if (jump_code == RISCV_INSN_ADDI) {
+
+                               /*
+                                * When the trampoland is initialized, three instructions are as follows:
+                                *
+                                * "auipc t0,imm20"
+                                * "addi t0,imm12"
+                                * "jalr t1,0(t0)"
+                                *
+                                * First, use the "jalr t1,imm12(t0)" replace the "addi t0,imm12"
+                                * To ensure that two instructions are caused in the case of multiple nuclear
+                                * conditions to lead to unknown errors.
+                                */
+
+                               make_call_t0(insn, func, call);
+                               patch_text_nosync(insn+sizeof(call[0]), &call[1], sizeof(call[1]));
+
+                               /*
+                                * After the last dynamic adjustment, the instructions are as follows:
+                                *
+                                * "auipc t0,imm20"
+                                * "jalr t1,imm12(t0)"
+                                * "jalr t1,0(t0)"
+                                *
+                                * The first two instructions have guaranteed that the third instruction will
+                                * not be ordered, so the third instruction can be updated.After the update is completed,
+                                * the second instruction can update to make the third jump instruction take effect.
+                                *
+                                * Use the "auipc t0,imm20" replace "jalr t0,imm12",the third jump instruction will take effect.
+                                */
+
+                               make_call_t0(insn+sizeof(call[0]), func, call);
+                               patch_text_nosync(insn+sizeof(call), &call[1], sizeof(call[1]));
+                               patch_text_nosync(insn+sizeof(call[0]), &call[0], sizeof(call[0]));
+
+                       }else if (jump_code == RISCV_INSN_AUIPC) {
+
+                               /*
+                                * After the last dynamic adjustment, the instructions are as follows:
+                                *
+                                * "auipc t0,imm20"
+                                * "auipc t0,imm20"
+                                * "jalr t1,imm12(t0)"
+                                *
+                                * The first auipc instruction will not take effect and can be updated easily.
+                                *
+                                * After updating the first auipc instruction, update the second auipc instruction
+                                * to the jalr instruction.
+                                */
+
+                               make_call_t0(insn, func, call);
+                               patch_text_nosync(insn, &call[0], sizeof(call[0]));
+                               patch_text_nosync(insn+sizeof(call[0]), &call[1], sizeof(call[1]));
+
+                       } else if (jump_code == RISCV_INSN_JALR) {
+
+                               /*
+                                * After the last dynamic adjustment, the instructions are as follows:
+                                *
+                                * "auipc t0,imm20"
+                                * "jalr t1,imm12(t0)"
+                                * "jalr t1,imm12(t0)"
+                                *
+                                * This looks like "jump_code == RISCV_INSN_ADDI".
+                                */
+
+                               make_call_t0(insn+sizeof(call[0]), func, call);
+                               patch_text_nosync(insn+sizeof(call), &call[1], sizeof(call[1]));
+                               patch_text_nosync(insn+sizeof(call[0]), &call[0], sizeof(call[0]));
+                       }
+                       break;
+               case RET://If func is NULL, the upgrade is not performed
+                       break;
+       }
+}

```

## Static Call 功能验证

### 基础功能验证

通过以下代码，可以验证3次蹦床升级是可以正常生效的。

```c
diff --git a/drivers/char/char_static.c b/drivers/char/char_static.c
new file mode 100644
index 000000000000..4901cc73da1f
--- /dev/null
+++ b/drivers/char/char_static.c
@@ -0,0 +1,93 @@
+#include <linux/kernel.h>
+#include <linux/module.h>
+#include <linux/static_call.h>
+int my_func(void)
+{
+    printk("Hello, world!\n");
+    return 0;
+}
+int my_func1(void)
+{
+    printk("Hello, world!1\n");
+    return 0;
+}
+int my_func2(void)
+{
+    printk("Hello, world!2\n");
+    return 0;
+}
+int my_func3(void)
+{
+    printk("Hello, world!3\n");
+    return 0;
+}
+DEFINE_STATIC_CALL(my_call, my_func);
+void* my_call;
+static int __init my_init(void)
+{
+
+   static_call(my_call)();
+   static_call_update(my_call, my_func);
+   printk("after update");
+
+    static_call(my_call)();
+    static_call_update(my_call, my_func1);
+    static_call(my_call)();
+    static_call_update(my_call, my_func2);
+    static_call(my_call)();
+    static_call_update(my_call, my_func3);
+    static_call(my_call)();
+    printk("static call end\n");
+    return 0;
+}
+
+static void __exit my_exit(void)
+{
+    static_call_update(my_call, NULL);
+}
+
+module_init(my_init);
+module_exit(my_exit);
+MODULE_LICENSE("GPL");

```

### code model: medlow & medany

> -mcmodel=medlow
>
> Generate code for the medium-low code model. The program and its statically defined symbols must lie within a single 2 GiB address range and must lie between absolute addresses -2 GiB and +2 GiB. Programs can be statically or dynamically linked. This is the default code model.

从编译结果来看，-mcmodel=medlow 使用 LUI 指令取符号地址的高20位。LUI 配合其它包含低12位立即数的指令后，可以访问的地址空间是 -2GiB ～ 2GiB。

对于 RV32，就是 0x00000000 ~ 0xFFFFFFFF，就是说可以访问任意地址了。

然而对于 RV64 而言，能访问的就是 0x0000000000000000 ~ 0x000000007FFFFFFF，以及 0xFFFFFFFF800000000 ~ 0xFFFFFFFFFFFFFFFF 这两个区域，前一个区域即 +2GiB 的地址空间，后一个区域即 -2GiB 的地址空间。其它地址空间就访问不到了。

![image-20230422000013757](images/static-call/image-20230422000013757.png)

> -mcmodel=medany
> Generate code for the medium-any code model. The program and its statically defined symbols must be within any single 2 GiB address range. Programs can be statically or dynamically linked.

从编译结果来看，-mcmodel=medany 使用 AUIPC 指令取符号地址的高20位。AUIPC 配合其它包含低12位立即数的指令后，可以访问当前 PC 的前后2GiB (PC - 2GiB ~ PC + 2GiB)的地址空间。

对于 RV32，能访问的还是 0x00000000 ~ 0xFFFFFFFF 这个区间，也是访问任意地址。

然而对于RV64，取决于当前 PC 值，能访问到是 PC - 2GiB 到 PC + 2GiB 这个地址空间。假设当前 PC 是 0x1000000000000000，那么能访问的地址范围是 0x0000000080000000 ~ 0x100000007FFFFFFF。假设当前 PC 是 0xA000000000000000，那么能访问的地址范围是0x9000000080000000~0xA00000007FFFFFFF。

![image-20230420233321330](images/static-call/image-20230420233321330.png)

反编译vmlinux：

```
build/riscv64/virt/linux/v6.3-rc4/vmlinux:     file format elf64-littleriscv

Disassembly of section .head.text:

ffffffff80000000 <_start>:
ffffffff80000000:       5a4d                    li      s4,-13
ffffffff80000002:       0d20106f                j       ffffffff800010d4 <_start_kernel>
ffffffff80000006:       0001                    nop
ffffffff80000008:       0000                    unimp
...
ffffffff80a3887e:       60e2                    ld      ra,24(sp)
ffffffff80a38880:       6442                    ld      s0,16(sp)
ffffffff80a38882:       64a2                    ld      s1,8(sp)
ffffffff80a38884:       6105                    addi    sp,sp,32
ffffffff80a38886:       8082                    ret
```

### kalsr测试

kalsr是对内核地址的随机化，可以防止攻击。

需要打以下补丁进行验证：

* PATCH-v9-0-1-riscv-Allow-to-downgrade-paging-mode-from-the-command-line.mbox
* PATCH-v2-0-4-riscv-Introduce-KASLR.mbox

该测试较为复杂，待完成inline版本后进行验证。

## 小结

本文讲了 Static Call 在risc-v 架构下的具体实现，后续需要进一步对补丁进行测试，以及inline版本的实现。

补丁地址：[[RFC PATCH] riscv: Add static call implementation][022]

后续工作：inline版本开发，kalsr测试验证。

## 参考资料

- [Static calls [LWN.net]][012]
[Static Call 系列（2）：retpolines][007]
- [retpoline: 原理与部署（terenceli.github.io）][020]
- [Linux Kernel 5.10 Introduces Static Calls to Prevent Speculative Execution Attacks - The New Stack][017]
- [Retpoline - caijiqhx notes][014]
- [Avoiding retpolines with static calls [LWN.net]][003]
- [[PATCH v2 02/13] static_call: Add basic static call infrastructure - Peter Zijlstra (kernel.org)][008]
- [[PATCH v2 03/13] static_call: Add inline static call infrastructure - Peter Zijlstra (kernel.org)][009]
- [[PATCH v2 05/13] x86/static_call: Add out-of-line static call implementation - Peter Zijlstra (kernel.org)][010]
- [[PATCH v2 06/13] x86/static_call: Add inline static call implementation for x86-64 - Peter Zijlstra (kernel.org)][011]
- [通过操作 Section 为 Linux ELF 程序新增数据][005]
- [arm 汇编指令 - cogitoergosum - 博客园（cnblogs.com）][018]
- [arm 汇编语法 - 简书（jianshu.com）][019]
- [eBPF 动态观测之指令跳板 | fuweid][006]
- [探秘 ftrace - Kernel Exploring (gitbook.io)][016]
- [Linux static_call - caijiqhx notes][015]
- [[RFC PATCH] riscv: Add static call implementation][022]

[002]: https://lwn.net/Articles/774743/
[003]: https://lwn.net/Articles/815908/
[004]: https://tinylab.org/missing-features-tools-for-riscv-part2/
[005]: https://tinylab.org/elf-sections/
[006]: https://fuweid.com/post/2022-bpf-kprobe-fentry-poke/
[007]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20221107-static-call-part2-retpoline.md
[008]: https://lore.kernel.org/lkml/20191007083830.64667428.5@infradead.org/
[009]: https://lore.kernel.org/lkml/20191007083830.70301561.0@infradead.org/
[010]: https://lore.kernel.org/lkml/20191007083830.81563732.6@infradead.org/
[011]: https://lore.kernel.org/lkml/20191007083830.87232371.5@infradead.org/
[012]: https://lwn.net/Articles/771209/
[014]: https://notes.caijiqhx.top/ucas/linux_kernel/retpoline/
[015]: https://notes.caijiqhx.top/ucas/linux_kernel/static_call/
[016]: https://richardweiyang-2.gitbook.io/kernel-exploring/00-index-3/04-ftrace_internal?from=timeline
[017]: https://thenewstack.io/linux-kernel-5-10-introduces-static-calls-to-prevent-speculative-execution-attacks/
[018]: https://www.cnblogs.com/hzijone/p/12005813.html
[019]: https://www.jianshu.com/p/1edc652351f4
[020]: http://terenceli.github.io/技术/2018/03/24/retpoline
[021]: https://zhuanlan.zhihu.com/p/438616789
[022]: https://lore.kernel.org/linux-riscv/tencent_A8A256967B654625AEE1DB222514B0613B07@qq.com/
