> Corrector: [TinyCorrect](https://gitee.com/tinylab/tinycorrect) v0.1 - [spaces codeinline urls pangu autocorrect]<br/>
> Author:    Kepontry <Kepontry@163.com><br/>
> Date:      2023/4/25<br/>
> Revisor:   Falcon <falcon@tinylab.org>; Walimis <walimis@walimis.org><br/>
> Project:   [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)<br/>
> Proposal:  [VisionFive 2 开发板软硬件评测及软件 gap 分析](https://gitee.com/tinylab/riscv-linux/issues/I64ESM)<br/>
> Sponsor:   PLCT Lab, ISCAS

# 基于 RISC-V SoC JH7110 的 Cache 容量推测

## 简介

### Cache

Cache 又被称为缓存，位于 CPU 和内存之间，容量小，速度快。它利用程序的时间和空间局部性，缓存常用的内存数据，从而提高数据访问速度。常见的 Cache 映射方式有直接映射、全相联和组相连几种。由于 Cache 容量远小于内存，所以容易出现映射冲突，即两个不同的内存地址映射到同一 Cache 行。直接映射是指每一个内存块只能映射到 Cache 中的一行，如果该行被占用，则必须先将旧数据换出。组相联是指将 Cache 分成 k 组（k 通常为 2 的幂），每个内存块能映射到 Cache 中的 k 行，只有当 k 行都被占用时，才必须将某行的旧数据换出。全相联是指每一个内存块都可以映射到 Cache 中的任意一行。一般来说，相联度越高，缓存的性能和命中率也会相应提高，但实现的复杂度与开销也随之上升。本次测试的 JH7110 SoC 拥有 32KB 的 L1 DCache 和 2MB 的 L2 Cache，其中 L1 DCache 采用 4 路组相连映射。

### 存储器山

存储器山（Memory Mountain）是《深入理解计算机系统》（CSAPP）一书中的实验。该实验以不同步长（相邻访存的地址差）遍历不同大小的数组（工作集），观测访存带宽的变化。当数组增大到大于某一级 Cache 容量时，该级 Cache 将产生频繁的换入换出。极端情况下，若工作集比 Cache 容量多一个元素，且 Cache 采用 LRU 替换策略，在不断遍历工作集的过程中，每一个新换入该级 Cache 中的元素都将替换掉下一次即将访问的元素。这会导致大量的缓存缺失，增加访存延迟，从而使访存带宽骤降。

存储器山图以步长和工作集大小为 X 和 Y 轴，访存带宽为 Z 轴，可以清晰地展示访存带宽的下降程度。通过观察每一个骤降点，可以推测出各级缓存的容量。测量时最大工作集应设置为大于末级缓存容量的值。

## Cache 容量推测

### 源码获取与编译

原书中提供了存储器山的 [测量代码][001]，但默认只支持 Alpha 和 X86 架构。为了在 RISC-V 上运行，我对它进行了移植，并合入 [GitHub 仓库][006] 中。执行以下命令以获取代码并编译出可执行文件 mountain。实验前最好关闭 CPU 的数据预取功能，以减小对实验结果的影响。关闭数据预取的方法可以参见 [这篇文章][005]，由于本次实验现象较为明确，受影响小，所以没有尝试关闭预取。

```shell
$ git clone https://github.com/fabiensanglard/CpuCacheMountainViewer.git
$ cd CpuCacheMountainViewer
$ make ARCH=riscv
```

### 源码移植简介

原始代码默认只支持 Alpha 和 X86 架构，为了使其支持 RISC-V 架构，需要实现 `start_counter` 和 `get_counter` 两个函数，分别用于启动计时器和计时结束后获取计数值。高精度时间的获取有两种方式，它们在测量时长为 1 秒的休眠时，误差都在万分之一秒内。第一种方式是直接调用 `clock_gettime` 函数，从 timer 驱动中获取计数器值，代码如下面所示，其中 `get_counter` 函数的返回值类型为 double，与其他架构保持一致。

```C
static struct timespec time1 = {0, 0};
static struct timespec time2 = {0, 0};
void start_counter() {
    clock_gettime(CLOCK_REALTIME, &time1);
}

double get_counter() {
    clock_gettime(CLOCK_REALTIME, &time2);
    return (time2.tv_sec - time1.tv_sec) + \
        (double)(time2.tv_nsec - time1.tv_nsec) / 1000000000;
}
```

第二种方式是从硬件时钟计数器中获取。RISC-V 指令集定义的时钟计数器有 cycle, time 和 instret 三个，分别统计周期数量、实时时钟计数与执行指令数。驱动 time 计数器的是板上固定频率的晶振，计时较为稳定，知道晶振频率后，可以准确换算 CPU 执行了多长时间。一个简单的实现代码如下，通过调用 `get_cycle` 函数获取高精度时间，从而实现 `start_counter` 和 `get_counter` 函数的功能。在 `get_cycle` 函数中，使用 csrr 指令读取 64 位 CSR 寄存器 time 的值到 val 变量中。如果 CPU 为 32 位，则分别读取 time 和 timeh 寄存器的值，作为 val 变量的低 32 位和高 32 位。如果在读取过程中发现 timeh 的值发生变化（低位进位），则重读 timeh 的值。

```C
static unsigned long long start_cycle;
void start_counter() {
    start_cycle = get_cycle();
}

double get_counter() {
    return (double)(get_cycle() - start_cycle);
}

unsigned long long get_cycle() {
    static unsigned long long val = 0;

#if __riscv_xlen == 32
    static unsigned long hi, hi1, lo;

    do {
        __asm__ volatile("csrr %0, timeh" : "=r"(hi));
        __asm__ volatile("csrr %0, time" : "=r"(lo));
        __asm__ volatile("csrr %0, timeh" : "=r"(hi1));
        /* hi != hi1 means `time` overflow during we get value,
         * so we must retry. */
    } while (hi != hi1);

    val = ((unsigned long long)hi << 32) | lo;
#else
    __asm__ volatile("csrr %0, time" : "=r"(val));
#endif

    return val;
}
```

### 结果可视化

运行程序，程序初始化阶段会进行时钟频率测量，即休眠 2s，用测量到的计数器差值除以时间。从下面的 log 可以看到，实际频率与理论相符，为 4MHz。下表展示了工作集大小从 8k 到 128M，步长从 1 到 31 的访存带宽（MB/s）。其中，步长不取偶数值是为了防止不同缓存路利用不均的情况发生，从而影响 Cache 容量的测量。在工作集大小为 64k 和 4M 时，访存带宽相对于 32k 和 2M 出现了大幅下降，所以推测该 CPU 的 L1 数据 Cache 容量为 32k，L2 Cache 容量为 2M，经验证与实际相符。

```shell
$ ./mountain
Clock frequency is approx. 4.0 MHz
Memory mountain (MB/sec)
		s1	s3	s5	s7	s9	s11	s13	s15	s17	s19	s21	s23	s25	s27	s29	s31
128m	296	260	232	212	199	181	167	154	145	136	80	39	39	41	48	96
64m		295	260	232	212	199	181	165	153	144	135	79	39	39	41	48	95
32m		295	260	231	211	198	179	164	152	143	134	79	39	39	41	48	94
16m		295	259	230	211	196	177	160	148	141	132	77	39	39	41	48	92
8m		295	258	230	210	192	173	153	143	138	128	75	40	40	41	49	89
4m		295	257	229	209	189	166	145	135	133	123	78	53	59	50	59	104
2m		298	267	241	222	203	182	165	152	145	138	126	143	154	118	137	160
1024k	300	271	247	229	212	197	183	171	166	166	165	165	165	165	165	165
512k	299	270	247	229	212	196	183	171	166	165	165	165	165	165	165	165
256k	299	270	246	228	212	196	182	171	165	165	165	165	164	164	164	164
128k	299	270	246	228	211	195	182	170	164	164	163	164	163	163	163	163
64k		299	270	245	227	210	195	180	170	164	162	171	175	187	183	201	184
32k		311	301	295	288	280	271	265	257	266	276	271	259	262	285	301	264
16k		314	312	312	302	303	298	296	291	296	287	284	285	291	269	282	302
8k		312	303	312	293	303	298	280	273	275	287	260	285	262	242	282	264
```

下图是使用表中数据绘制的存储器山图，从中可以清晰地看到位于 32k 和 2M 处的两个断面。同时，步长越大，空间局部性越差，带宽也随之逐步下降。

![Image Description](images/vf2-mem-mountain/vf2-mem-mountain.png)

### 编程启示

从上述结果中可以看出，当工作集增大时，性能逐渐下降，当跨越缓存容量边界时的下降尤其明显，所以在编程中要将工作集控制在合理的大小内。当工作集很难继续压缩时，可以再考虑数据访问局部性方面的优化，比较典型的示例是用二维数组的行优先遍历而不是列优先遍历。从结果中还可以看出，当数据访问步长变大时，性能也会下降，所以编程中也要考虑访问步长的影响。

例如在以下两个结构体定义 test1 和 test2 中，在进行结构体数组 arr1 和 arr2 遍历时，使用 test1 的性能更好。这是因为虽然两个结构体的成员类型与数量相同，但结构体成员变量默认会 4 字节对齐。由于 test1 中的短整型变量 a 和 c 是连续的，所以共占 4 字节。而在 test2 中，这两个变量不是连续的，所以需要分别按 4 字节对齐，共占 8 字节。通过对比可以发现，test1 的工作集更小，数据访问步长也更小。

```C
// test.c
#include <stdio.h>
#define K 1000
#define M 1000*1000
#define SIZE 10*M
struct test1 {
    short int a;
    short int c;
    int b;
    int d;
};
struct test1 arr1[SIZE];

struct test2 {
    short int a;
    int b;
    short int c;
    int d;
};
struct test2 arr2[SIZE];
int main(){
    for (int i = 0; i < SIZE; i++) {
        arr1[i].a = 1;
        arr2[i].a = 1;
    }
    return 0;
}
```

通过 gdb 中的变量查看功能可以印证上述观点。首先在编译时加入 `-g` 选项，以保留调试信息。在进入 gdb 后，输入 r 让程序正常跑完。最后使用 p 命令观察各变量的地址。通过观察可以发现 arr1[0] 结构体的 a 和 c 变量占用 2 字节的存储空间，arr2[0] 结构体的 a 和 c 分别占用 4 字节的存储空间。

```shell
$ gcc test.c -g -o test
$ gdb --args ./test
Reading symbols from ./test...
(gdb) r
[Inferior 1 (process 759) exited normally]
(gdb) p &arr1[0]->a
$1 = (short *) 0x2aaaaac050 <arr1>
(gdb) p &arr1[0]->b
$2 = (int *) 0x2aaaaac054 <arr1+4>
(gdb) p &arr1[0]->c
$3 = (short *) 0x2aaaaac052 <arr1+2>
(gdb) p &arr1[0]->d
$4 = (int *) 0x2aaaaac058 <arr1+8>
(gdb) p &arr2[0]->a
$5 = (short *) 0x2ab1d1ce50 <arr2>
(gdb) p &arr2[0]->b
$6 = (int *) 0x2ab1d1ce54 <arr2+4>
(gdb) p &arr2[0]->c
$7 = (short *) 0x2ab1d1ce58 <arr2+8>
(gdb) p &arr2[0]->d
$8 = (int *) 0x2ab1d1ce5c <arr2+12>
(gdb) q
```

## 总结

本文基于经典的存储器山理论，在理解源码的基础上，将其移植到 RISC-V 平台，并通过实验正确推测出了 JH7110 的两级 Cache 容量。针对工作集增大和数据访问步长增大带来的性能下降问题，本文给出了代码建议与一个简单示例。

## 参考资料

- [深入理解计算机系统课程主页][002]
- [JH7110 CPU 子系统手册][003]
- [SiFive Freedom Metal 项目][004]
- [深入理解指针—>结构体里的成员数组和指针][007]

[001]: http://csapp.cs.cmu.edu/2e/mountain.tar
[002]: http://csapp.cs.cmu.edu/2e/students.html
[003]: https://doc-en.rvspace.org/JH7110/Datasheet/JH7110_DS/c_u74_quad_core.html
[004]: https://github.com/sifive/freedom-metal/
[005]: https://gitee.com/tinylab/riscv-linux/blob/master/articles/20230509-vf2-hw-prefetch.md
[006]: https://github.com/fabiensanglard/CpuCacheMountainViewer
[007]: https://www.cnblogs.com/tham/p/6827260.html
