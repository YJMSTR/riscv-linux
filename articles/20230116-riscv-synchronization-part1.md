> Corrector: [TinyCorrect](https://gitee.com/tinylab/tinycorrect) v0.1 - [spaces codeinline tables pangu autocorrect epw]<br/>
> Author:    Kyrie jia bokai021013@gmail.com<br/>
> Date:      2023/01/16<br/>
> Revisor:   Falcon falcon@tinylab.org<br/>
> Project:   [RISC-V Linux 内核剖析](https://gitee.com/tinylab/riscv-linux)<br/>
> Proposal:  [RISC-V Linux Synchronization 技术调研与分析](https://gitee.com/tinylab/riscv-linux/issues/I5MUAN)<br/>
> Sponsor:   PLCT Lab, ISCAS

# RISC-V 同步与通信机制

## 前言

本文将剖析 RISC-V 架构下 Linux 进程间通信的三种方式，并给出实验示例和分析。

## Linux 进程间通信

**进程间通信**（interprocess communication，简称 IPC）通常指不同进程之间的通信，包括互斥，同步和管理共享数据等。

> **进程**是系统进行资源分配的基本单位，通常由 PCB（进程控制块），数据段和代码段组成。

每个进程都处在自己的地址空间内，相互通信比较困难，幸而 Linux 内核中提供了多种进程间通信的机制。

其中使用较多的主要有以下几种：

- 管道
- 信号
- 共享内存
- 消息队列
- 信号量
- 套接字

### 管道

管道又可以分为**无名管道**（pipe）和**有名管道**（named pipe)，无名管道可用于具有亲缘关系的进程间的通讯，有名管道除了具有和无名管道相似的功能外，还允许无亲缘关系的进程使用。

#### 无名管道

无名管道主要用于父子进程之间的通信。是通过一个进程向管道写入数据，另一个进程从管道读取数据来实现的。这意味着一个进程的输出可以作为另一个进程的输入。Linux 定义了 `pipe()` 实现无名管道的建立：

```
#include<unistd.h>

int pipe(int pipedes[2]);
```

该系统调用将创建一个无名管道用来单向通信，它需要传入两个描述符，第一个 `pipedes[0]` 连接到管道中读取数据，第二个 `pipedes[1]` 连接到管道中写入数据，成功执行返回 0，失败则返回-1。

示例如下：

```
#include<stdio.h>
#include<unistd.h>

int main() {
   int pipefds[2];
   int returnstatus;
   int pid;
   char writemessages[2][20]={"Hi", "Hello"};
   char readmessage[20];
   returnstatus = pipe(pipefds);
   if (returnstatus == -1) {
      printf("Unable to create pipe\n");
      return 1;
   }
   pid = fork();

   // Child process
   if (pid == 0) {
      read(pipefds[0], readmessage, sizeof(readmessage));
      printf("Child Process - Reading from pipe – Message 1 is %s\n", readmessage);
      read(pipefds[0], readmessage, sizeof(readmessage));
      printf("Child Process - Reading from pipe – Message 2 is %s\n", readmessage);
   } else {
   //Parent process
      printf("Parent Process - Writing to pipe - Message 1 is %s\n", writemessages[0]);
      write(pipefds[1], writemessages[0], sizeof(writemessages[0]));
      printf("Parent Process - Writing to pipe - Message 2 is %s\n", writemessages[1]);
      write(pipefds[1], writemessages[1], sizeof(writemessages[1]));
   }
   return 0;
}
```

首先创建管道，然后创建子进程，父进程利用 `pipefds[1]` 描述符向管道写入数据，子进程用 `pipefds[0]` 描述符从管道中读取数据，重复该过程就可以实现父子进程之间的通信。

结果输出如下：

```
Parent Process - Writing to pipe - Message 1 is Hi
Parent Process - Writing to pipe - Message 2 is Hello
Child Process - Reading from pipe – Message 1 is Hi
Child Process - Reading from pipe – Message 2 is Hello
```

上述管道只能实现单向通信，即同一时间只有一个进程可以传输数据，那如果父子进程需要同时从管道中读取数据怎么办呢？其实也很简单，可以搭建两个管道，一个用来读取，一个用来写入，就可以实现双向通信了。有一点需要注意的是，如果进程确定该管道用来读取数据，那么应该关闭写入端口，而利用另一个管道进行写入的同时关闭其读取端口。

示例如下：

```
#include<stdio.h>
#include<unistd.h>

int main() {
   int pipefds1[2], pipefds2[2];
   int returnstatus1, returnstatus2;
   int pid;
   char pipe1writemessage[20] = "Hi";
   char pipe2writemessage[20] = "Hello";
   char readmessage[20];
   returnstatus1 = pipe(pipefds1);

   if (returnstatus1 == -1) {
      printf("Unable to create pipe 1 \n");
      return 1;
   }
   returnstatus2 = pipe(pipefds2);

   if (returnstatus2 == -1) {
      printf("Unable to create pipe 2 \n");
      return 1;
   }
   pid = fork();

   if (pid != 0) // Parent process {
      close(pipefds1[0]);
      close(pipefds2[1]);
      printf("In Parent: Writing to pipe 1 – Message is %s\n", pipe1writemessage);
      write(pipefds1[1], pipe1writemessage, sizeof(pipe1writemessage));
      read(pipefds2[0], readmessage, sizeof(readmessage));
      printf("In Parent: Reading from pipe 2 – Message is %s\n", readmessage);
   } else { //child process
      close(pipefds1[1]);
      close(pipefds2[0]);
      read(pipefds1[0], readmessage, sizeof(readmessage));
      printf("In Child: Reading from pipe 1 – Message is %s\n", readmessage);
      printf("In Child: Writing to pipe 2 – Message is %s\n", pipe2writemessage);
      write(pipefds2[1], pipe2writemessage, sizeof(pipe2writemessage));
   }
   return 0;
}
```

结果输出如下：

```
In Parent: Writing to pipe 1 – Message is Hi
In Child: Reading from pipe 1 – Message is Hi
In Child: Writing to pipe 2 – Message is Hello
In Parent: Reading from pipe 2 – Message is Hello
```

可以看到父进程利用管道 1 写入数据供子进程读取，子进程利用管道 2 写入数据供父进程读取，同时关闭不需要的端口，可以实现同时传输数据。

#### 有名管道

有名管道可以实现无亲缘关系的进程之间的通信，并且支持双向通信，不需要像无名管道那样建立两个管道，仅使用一个管道就能同时传输数据。

```
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int mknod(const char *pathname, mode_t mode, dev_t dev);
```

有名管道也叫 FIFO，是一种特殊的文件，可以由上述系统调用创建出来。其中 `pathname` 是传入的文件相对路径，`mode` 是特定的文件模式，不同文件类型有不同的模式，具体如下表所示。`dev` 是特定的设备信息，如主设备编号等。

| **文件类型** | **描述**          |
|--------------|-------------------|
| S_IFREG      | Regular file      |
| S_IFBLK      | block special     |
| S_IFCHR      | character special |
| S_IFIFO      | FIFO special      |
| S_IFDIR      | Directory         |
| S_IFLNK      | Symbolic Link     |

| **文件模式** | **描述**                              |
|--------------|---------------------------------------|
| S_IRWXU      | Read, write, execute/search by owner  |
| S_IRUSR      | Read permission, owner                |
| S_IWUSR      | Write permission, owner               |
| S_IXUSR      | Execute/search permission, owner      |
| S_IRWXG      | Read, write, execute/search by group  |
| S_IRGRP      | Read permission, group                |
| S_IWGRP      | Write permission, group               |
| S_IXGRP      | Execute/search permission, group      |
| S_IRWXO      | Read, write, execute/search by others |
| S_IROTH      | Read permission, others               |
| S_IWOTH      | Write permission, others              |

文件模式也可以用八进制来表示，例如 0XYZ，其中 X 表示所有者（owner），Y 表示组（group），Z 表示其他（others）。X、Y、Z 取值范围为 0-7。read、write 和 execute 分别为 4、2、1。假如我们设定 `mode` 是 0640，那么所有者可以读写，同组进程也可以读，其他进程则没有任何权限。若成功执行返回 0，失败则返回 -1。

下面我们用有名管道实现单向通信，假定 client 进程用来接受用户输入的数据，然后传输给 server 并输出数据，示例如下：

**fifoserver.c**

```
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define FIFO_FILE "MYFIFO"
int main() {
   int fd;
   char readbuf[80];
   char end[10];
   int to_end;
   int read_bytes;

   mknod(FIFO_FILE, S_IFIFO|0640, 0);
   strcpy(end, "end");
   while(1) {
      fd = open(FIFO_FILE, O_RDONLY);
      read_bytes = read(fd, readbuf, sizeof(readbuf));
      readbuf[read_bytes] = '\0';
      printf("Received string: \"%s\" and length is %d\n", readbuf, (int)strlen(readbuf));
      to_end = strcmp(readbuf, end);
      if (to_end == 0) {
         close(fd);
         break;
      }
   }
   return 0;
}
```

这是 server 进程，首先利用 `mknod` 创建有名管道，其中 `mode` 指定其有读写权限，然后等待管道数据，只要不是 `end`，就打印出信息。

**fifoclient.c**

```
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define FIFO_FILE "MYFIFO"
int main() {
   int fd;
   int end_process;
   int stringlen;
   char readbuf[80];
   char end_str[5];
   fd = open(FIFO_FILE, O_CREAT|O_WRONLY);
   strcpy(end_str, "end");

   while (1) {
      printf("Enter string: ");
      fgets(readbuf, sizeof(readbuf), stdin);
      stringlen = strlen(readbuf);
      readbuf[stringlen - 1] = '\0';
      end_process = strcmp(readbuf, end_str);

      if (end_process != 0) {
         write(fd, readbuf, strlen(readbuf));
         printf("Sent string: \"%s\" and string length is %d\n", readbuf, (int)strlen(readbuf));
      } else {
         write(fd, readbuf, strlen(readbuf));
         printf("Sent string: \"%s\" and string length is %d\n", readbuf, (int)strlen(readbuf));
         close(fd);
         break;
      }
   }
   return 0;
}
```

这是 client 进程，可以打开有名管道写入数据，在循环中等待用户输入数据，只要不是 `end`,就写入管道，如果是 `end`，写入后关闭管道，停止循环。

输出如下：

**client**

```
Enter string: this is string 1
Sent string: "this is string 1" and string length is 16
Enter string: fifo test
Sent string: "fifo test" and string length is 9
Enter string: fifo client and server
Sent string: "fifo client and server" and string length is 22
Enter string: end
Sent string: "end" and string length is 3
```

**server**

```
Received string: "this is string 1" and length is 16
Received string: "fifo test" and length is 9
Received string: "fifo client and server" and length is 22
Received string: "end" and length is 3
```

### 信号

信号（signal）用来通知进程某个事件的出现，也被叫做**软件中断**（software interrupt），由于很难预测它的出现，因此也被称为**异步事件**（asynchronous event）。信号可以由一个数字或一个名字区分，名字通常以 SIG 开头。

当信号产生时，系统会执行一个默认的行为（default action），这时可以将默认行为替换为自定义的行为，也可以忽略该信号不执行。只有 SIGSTOP，SIGKILL 两个信号既不能忽略也不能替换成自定义行为。

#### 系统调用

Linux 定义了两个系统调用来处理信号：`signal()` 和 `sigaction()`。

```
#include <signal.h>

typedef void (*sighandler_t) (int);
sighandler_t signal(int signum, sighandler_t handler);
int sigaction(int signum, const struct sigaction *act, struct sigaction *oldact)
```

系统调用 `signal()` 将在信号产生时调用 signum 中已注册的处理程序，成功执行时返回一个函数的地址，这个函数需要传入一个 int 整型参数，并且没有返回值。如果发生错误，此调用将返回 SIG_ERR。

虽然使用 `signal()` 可以调用用户注册的信号处理程序，但不能进行诸如屏蔽应该阻塞的信号、修改信号的行为和其他功能之类的微调。`sigaction()` 解决了这个问题，它可以用来检查或修改一个信号的行为。如果 `act` 不为空，则从该 `act` 加载信号 signum 的新行为，如果 `oldact` 不为空，则将上一个行为保存在 `oldact` 中。

#### 模拟信号应用

下面我们看一下信号的具体应用：

```
#include<stdio.h>
#include<signal.h>
#include<stdlib.h>

void handler_dividebyzero(int signum);

int main() {
   int result;
   int v1, v2;
   void (*handlerReturn)(int);
   handlerReturn = signal(SIGFPE, handler_dividebyzero);
   if (handlerReturn == SIG_ERR) {
      perror("Signal Error: ");
      return 1;
   }
   v1 = 1;
   v2 = 0;
   result = v1/v2;
   printf("Result of Divide by Zero is %d\n", result);
   return 0;
}

void handler_dividebyzero(int signum) {
   if (signum == SIGFPE) {
      printf("Received SIGFPE, Divide by Zero Exception\n");
      exit (0);
   }
   else
      printf("Received %d Signal\n", signum);
      return;
}
```

如上可知，当系统执行除 0 运算时，会 core dump 并且抛出浮点数异常：`Floating point exception (core dumped)`，这时就会产生信号，进入用户定义的信号处理程序，也就是 `handler` 程序。

输出如下：

```
Received SIGFPE, Divide by Zero Exception
```

Linux 也提供了一个系统调用 `raise()` 可以用编程方式产生信号，在产生信号后，当前进程的执行会被暂停，之后需要的话可以发出 `SIGCONT` 信号恢复该进程。下面的示例将使用 `raise()` 产生一个 `SIGSTOP` 信号，然后用 `SIGCONT` 恢复。

```
#include<stdio.h>
#include<signal.h>
#include <sys/types.h>
#include <unistd.h>

int main() {
   pid_t pid;
   printf("Testing SIGSTOP\n");
   pid = getpid();
   raise(SIGSTOP);
   printf("Received signal SIGCONT\n");
   return 0;
}
```

在另一个终端执行：

```
kill -SIGCONT 30379
```

输出：

```
Testing SIGSTOP
[1]+ Stopped ./a.out

Received signal SIGCONT
[1]+ Done ./a.out
```

可以看到在收到 `SIGSTOP` 信号时，第一个进程被停止，直到第二个进程发出 `SIGCONT` 信号时，才重新恢复执行，输出后面的语句。

还有一些经常使用的信号如 `SIGINT`、`SIGQUIT`、`SIGTSTP` 等，对于它们的处理也跟上述方式有相似之处，就不一一列举了。

### 共享内存

共享内存（shared memory）是最有效的进程间通讯方式。一段共享内存通常由一个进程创建，但其它的进程也可以访问，使得多个进程可以访问统一的内存空间，不同进程可以及时看到对方进程中对共享内存中数据的更新。但这种通讯方式需要依靠某种同步机制，如互斥锁和信号量等才能实现进程同步。

对于一个共享内存，其实现采用的是引用计数的原理，当进程脱离共享存储区后，计数器减一，访问成功时，计数器加一，只有当计数器变为零时，才能被删除。当进程终止时，它所附加的共享存储区就会自动脱离。

#### 系统调用

首先是**创建共享内存**：

```
#include <sys/ipc.h>
#include <sys/shm.h>

int shmget(key_t key, size_t size, int shmflg)
```

该系统调用会创建或者分配一个共享内存段。

第一个参数 `key` 是用来标识 IPC 资源的。`key` 可以是任意值，也可以是从库函数 `ftok()` 中派生的值。

第二个参数 `size` 是需要申请共享内存的大小。在操作系统中，申请内存的最小单位为页，一页通常是 4k 字节，为了避免内存碎片，我们一般申请的内存大小为页的整数倍。

第三个参数 `shmflg`，如果要创建新的共享内存，需要使用 IPC_CREAT，IPC_EXCL，如果是已经存在的，可以使用 IPC_CREAT 或直接传 0。

然后是**挂接共享内存**：

```
#include <sys/types.h>
#include <sys/shm.h>

void * shmat(int shmid, const void *shmaddr, int shmflg)
```

该系统调用将一个共享内存段附加到调用进程的地址空间中。

第一个参数 `shmid` 是共享内存段的标识符，即 `shmget()` 的返回值。

第二个参数 `shmaddr` 指定附加地址。如果 `shmadr` 为空，则系统默认选择合适的地址来连接共享内存段。如果 `shmadr` 不为空并且 `shmflg` 中指定了 SHM_RND，则附加值等于 SHMLBA（下边界地址）的最近倍数的地址。

第三个参数 `shmflg` 指定所需的共享内存标志，例如 SHM_RND（舍入地址到 SHMLBA）或 SHM_EXEC（允许执行段的内容）或 SHM_RDONLY（附加段用于只读目的，默认情况下是读写）或 SHM_REMAP（替换 shmadr 指定的范围内的现有映射并持续到段的结束）。

**解除共享内存关联**：

```
#include <sys/types.h>
#include <sys/shm.h>

int shmdt(const void *shmaddr)
```

该系统调用将从调用进程指定地址解除与共享内存段的连接。

传入的 `shmaddr` 即指定地址，成功执行返回 0，并将 `shmid_ds` 结构体中的 shm_nattch 计数器减 1，出错返回-1。

**销毁共享内存**

```
#include <sys/ipc.h>
#include <sys/shm.h>

int shmctl(int shmid, int cmd, struct shmid_ds *buf)
```

该系统调用将删除共享内存段占用的空间。

第一个参数 `shmid` 是共享内存段的标识符。

第二个参数 `cmd` 是在共享内存段上执行所需控制操作的命令。有效参数如下：

- IPC_STAT：将 `shmid_ds` 结构体的每个成员的当前值复制到 buf 所指向的结构体中，该命令需要共享内存段的读权限。
- IPC_SET：设置用户 ID，所有者的组 ID，权限等。
- IPC_RMID：标记要销毁的段，只有在最后一个进程将该段分离后，才会销毁它。
- IPC_INFO：返回 buf 指向的结构体中共享内存段的限制和参数信息。
- SHM_INFO：返回一个 `SHM_INFO` 结构，包含共享内存段消耗的系统资源的信息。

第三个参数 `buf` 是指向结构体 `shmid_ds` 的共享内存结构的指针。

#### 模拟共享内存应用

我们创建两个进程，一个用于向共享内存写入数据 (shm_write.c)，另一个用于从共享内存读取数据 (shm_read.c)。有写入进程创建一个大小为 1K 的共享内存段并附加到地址空间中，写入进程将分 5 次把从' A '到' E '的字母重复写满共享内存。读进程将从共享内存中读取数据并输出，两者同时进行。

示例如下（代码来自 [进程通信入门教程 - 进程 - 共享内存][004]）：

**shm_write.c**

```
#include<stdio.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<sys/types.h>
#include<string.h>
#include<errno.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>

#define BUF_SIZE 1024
#define SHM_KEY 0x1234

struct shmseg {
   int cnt;
   int complete;
   char buf[BUF_SIZE];
};
int fill_buffer(char * bufptr, int size);

int main(int argc, char *argv[]) {
   int shmid, numtimes;
   struct shmseg *shmp;
   char *bufptr;
   int spaceavailable;
   shmid = shmget(SHM_KEY, sizeof(struct shmseg), 0644|IPC_CREAT);
   if (shmid == -1) {
      perror("Shared memory");
      return 1;
   }

   shmp = shmat(shmid, NULL, 0);
   if (shmp == (void *) -1) {
      perror("Shared memory attach");
      return 1;
   }

   bufptr = shmp->buf;
   spaceavailable = BUF_SIZE;
   for (numtimes = 0; numtimes < 5; numtimes++) {
      shmp->cnt = fill_buffer(bufptr, spaceavailable);
      shmp->complete = 0;
      printf("Writing Process: Shared Memory Write: Wrote %d bytes\n", shmp->cnt);
      bufptr = shmp->buf;
      spaceavailable = BUF_SIZE;
      sleep(3);
   }
   printf("Writing Process: Wrote %d times\n", numtimes);
   shmp->complete = 1;

   if (shmdt(shmp) == -1) {
      perror("shmdt");
      return 1;
   }

   if (shmctl(shmid, IPC_RMID, 0) == -1) {
      perror("shmctl");
      return 1;
   }
   printf("Writing Process: Complete\n");
   return 0;
}

int fill_buffer(char * bufptr, int size) {
   static char ch = 'A';
   int filled_count;
   memset(bufptr, ch, size - 1);
   bufptr[size-1] = '\0';
   if (ch > 122)
   ch = 65;
   if ( (ch >= 65) && (ch <= 122) ) {
      if ( (ch >= 91) && (ch <= 96) ) {
         ch = 65;
      }
   }
   filled_count = strlen(bufptr);
   ch++;
   return filled_count;
}
```

**shm_read.c**

```
#include<stdio.h>
#include<sys/ipc.h>
#include<sys/shm.h>
#include<sys/types.h>
#include<string.h>
#include<errno.h>
#include<stdlib.h>

#define BUF_SIZE 1024
#define SHM_KEY 0x1234

struct shmseg {
   int cnt;
   int complete;
   char buf[BUF_SIZE];
};

int main(int argc, char *argv[]) {
   int shmid;
   struct shmseg *shmp;
   shmid = shmget(SHM_KEY, sizeof(struct shmseg), 0644|IPC_CREAT);
   if (shmid == -1) {
      perror("Shared memory");
      return 1;
   }

   shmp = shmat(shmid, NULL, 0);
   if (shmp == (void *) -1) {
      perror("Shared memory attach");
      return 1;
   }

   while (shmp->complete != 1) {
      printf("segment contains : \n\"%s\"\n", shmp->buf);
      if (shmp->cnt == -1) {
         perror("read");
         return 1;
      }
      printf("Reading Process: Shared Memory: Read %d bytes\n", shmp->cnt);
      sleep(3);
   }
   printf("Reading Process: Reading Done, Detaching Shared Memory\n");
   if (shmdt(shmp) == -1) {
      perror("shmdt");
      return 1;
   }
   printf("Reading Process: Complete\n");
   return 0;
}
```

输出如下：

```
Writing Process: Shared Memory Write: Wrote 1023 bytes
Writing Process: Shared Memory Write: Wrote 1023 bytes
Writing Process: Shared Memory Write: Wrote 1023 bytes
Writing Process: Shared Memory Write: Wrote 1023 bytes
Writing Process: Shared Memory Write: Wrote 1023 bytes
Writing Process: Wrote 5 times
Writing Process: Complete
segment contains :
"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
Reading Process: Shared Memory: Read 1023 bytes
segment contains :
"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB
BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB"
Reading Process: Shared Memory: Read 1023 bytes
segment contains :
"CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"
Reading Process: Shared Memory: Read 1023 bytes
segment contains :
"DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD"
Reading Process: Shared Memory: Read 1023 bytes
segment contains :
"EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"
Reading Process: Shared Memory: Read 1023 bytes
Reading Process: Reading Done, Detaching Shared Memory
Reading Process: Complete
```

可以看到使用共享内存进行进程之间的通信是非常方便的，而且函数的接口也比较简单，数据的共享还使进程间的数据不用传送，而是直接访问内存，加快了程序的效率。

## 总结

本文分析了 RISC-V 架构下 Linux 进程间通信的三种方式，后续会介绍剩余的方式。

## 参考资料

* [RISC-V 源码][001]
* [Linux 进程间通信][002]
* [RISV-V Specifications][003]
* [进程通信入门教程 - 进程 - 共享内存][004]

[001]: https://github.com/torvalds/linux
[002]: https://cloud.tencent.com/developer/article/2139745?areaSource=104001.14&traceId=vUp6q62ytlsiP2P-vyZ8n
[003]: https://riscv.org/technical/specifications/
[004]: https://www.learnfk.com/process/inter-process-communication-shared-memory.html
